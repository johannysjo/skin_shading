#!/bin/bash

export SKIN_SHADING_ROOT=$(pwd)

cd $SKIN_SHADING_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$SKIN_SHADING_ROOT ../ && \

make -j4 && \
make install && \

cd ../bin && \
./skin_shading
