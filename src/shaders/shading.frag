#version 450

layout(binding = 0) uniform sampler2D u_base_color_gloss_texture;
layout(binding = 1) uniform sampler2D u_normal_texture;
layout(binding = 2) uniform sampler2D u_sss_emissive_texture;
layout(binding = 3) uniform sampler2D u_depth_texture;
layout(binding = 4) uniform sampler2D u_ssao_texture;
layout(binding = 5) uniform sampler2D u_shadow_map_texture;
layout(binding = 6) uniform sampler2D u_blue_noise_texture;
layout(binding = 7) uniform samplerCube u_cubemap_texture;

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
    vec4 camera_world_pos;
} u_scene;

layout(location = 0) uniform vec3 u_key_light_direction;
layout(location = 1) uniform float u_key_light_intensity;
layout(location = 2) uniform mat4 u_key_light_proj_from_world;
layout(location = 3) uniform mat4 u_key_light_proj_from_view;
layout(location = 4) uniform float u_sky_light_intensity;
layout(location = 5) uniform float u_exposure;
layout(location = 6) uniform bool u_shadows_enabled;
layout(location = 7) uniform vec2 u_shadow_slope_bias;
layout(location = 8) uniform float u_shadow_normal_bias;
layout(location = 9) uniform float u_pdf_radius;
layout(location = 10) uniform uint u_pdf_num_samples;
layout(location = 11) uniform vec3 u_scatter_color;
layout(location = 12) uniform bool u_translucency_enabled;
layout(location = 13) uniform float u_translucency_max_depth;
layout(location = 14) uniform uint u_frame_index;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 rt_diffuse;
layout(location = 1) out vec4 rt_specular;

#define PI 3.14159265358

vec3 srgb2lin(vec3 color)
{
    return pow(color, vec3(2.2));
}

vec3 world_pos_from_depth(float depth, vec2 texcoord, mat4 world_from_proj)
{
    vec4 ndc_pos = vec4(2.0 * texcoord - 1.0, 2.0 * depth - 1.0, 1.0);
    vec4 world_pos = world_from_proj * ndc_pos;
    world_pos.xyz /= world_pos.w;

    return world_pos.xyz;
}

float diffuse_wrap(vec3 N, vec3 L, float wrap)
{
    return max(0.0, (dot(N, L) + wrap) / ((1.0 + wrap) * (1.0 + wrap)));
}

float gloss_to_specular_power(float gloss)
{
    return pow(2.0, 10.0 * gloss + 1.0);
}

float specular_D_blinn_phong(vec3 N, vec3 H, float specular_power)
{
    return pow(max(0.0, dot(N, H)), specular_power) * (specular_power + 8.0) / 8.0;
}

float gloss_to_roughness(float gloss)
{
    return 1.0 - min(0.95, gloss);
}

float specular_D_ggx(vec3 N, vec3 H, float roughness)
{
	float a = roughness * roughness;
    float a2 = a * a;
    float N_dot_H = max(0.0, dot(N, H));
    float denom = N_dot_H * N_dot_H * (a2 - 1.0) + 1.0;
    return a2 / (denom * denom);
}

vec3 fresnel_schlick(vec3 R_F0, vec3 E, vec3 N)
{
    return R_F0 + (1.0 - R_F0) * pow(1.0 - max(0.0, dot(E, N)), 5.0);
}

vec3 fresnel_schlick_gloss(vec3 R_F0, vec3 E, vec3 N, float gloss)
{
    return R_F0 + (max(vec3(gloss), R_F0) - R_F0) * pow(1.0 - max(0.0, dot(E, N)), 5.0);
}

// Approximates glossy environment reflection by taking a single trilinearly interpolated sample of
// a mip-mapped cubemap texture. The mip level is selected to match the solid angle of the
// Blinn-Phong specular lobe for a given specular power.
// Reference: McGuire et al. Plausible Blinn-Phong Reflection of Standard Cube MIP-maps, 2013.
vec3 sample_radiance_envmap(samplerCube cubemap_texture, vec3 R, float specular_power)
{
    vec2 cubemap_res = textureSize(cubemap_texture, 0);
    float mip_level = log2(cubemap_res.x * sqrt(3.0)) - 0.5 * log2(specular_power + 1.0);
    return textureLod(cubemap_texture, R, mip_level).rgb;
}

vec3 ao_approx_bounce(float ao, vec3 base_color)
{
    return clamp(ao / (1.0 - 0.99 * base_color * (1.0 - ao)), 0.0, 1.0);
}

float ao_approx_specular_occlusion(float ao, vec3 N, vec3 V, float gloss)
{
    float N_dot_V = max(0.0, dot(N, V));
    return mix(ao, clamp(N_dot_V * N_dot_V + 2.0 * ao - 1.0, 0.0, 1.0), gloss);
}

vec2 sample_vogel_disk(uint i, uint num_samples, vec2 rnd)
{
    float r = sqrt((float(i) + rnd.x) / float(num_samples));
    float golden_angle = 2.4;
    float phi = float(i) * golden_angle + 2.0 * PI * rnd.y;
    float x = r * cos(phi);
    float y = r * sin(phi);

    return vec2(x, y);
}

float hash(float seed)
{
    return fract(sin(seed * 12.9899) * 43758.5453);
}

float sample_shadow_map(sampler2D shadow_map, vec3 world_pos, mat4 light_proj_from_world,
                        float bias, float radius, uint num_samples, vec2 rnd)
{
    vec4 shadow_map_pos = light_proj_from_world * vec4(world_pos, 1.0);
    shadow_map_pos.xyz = 0.5 * shadow_map_pos.xyz + 0.5;
    vec2 texel_size = 1.0 / textureSize(shadow_map, 0).xy;
    float visibility = 0.0;
    for (uint i = 0; i < num_samples; ++i) {
        vec2 offset = radius * texel_size * sample_vogel_disk(i, num_samples, rnd);
        if (shadow_map_pos.z - bias > texture(shadow_map, shadow_map_pos.xy + offset).r) {
            visibility += 1.0;
        }
    }
    visibility = 1.0 - visibility / float(num_samples);

    return visibility;
}

float estimate_thickness(sampler2D shadow_map, vec3 world_pos, vec3 world_normal,
                         mat4 light_proj_from_world, mat4 light_proj_from_view)
{
    // Shrink the surface slightly to prevent aliasing artifacts at projection boundaries, as
    // suggested by Jimenez et al. in Real-Time Realistic Skin Translucency, CGA, 2010.
    vec3 world_pos_shrinked = world_pos - 0.001 * world_normal;

    // Calculate the z-buffer depth difference between the projected world point and the closest
    // surface intersection as seen from the light source.
    vec4 shadow_map_pos = light_proj_from_world * vec4(world_pos_shrinked, 1.0);
    shadow_map_pos.xyz = 0.5 * shadow_map_pos.xyz + 0.5;
    float depth_diff = max(0.0, shadow_map_pos.z - texture(shadow_map, shadow_map_pos.xy).r);

    // Multiply the z-buffer depth difference with the diagonal element m[2][2] of the projection
    // matrix to obtain the thickness in view space units. Assumes that the projection matrix is
    // orthographic and that m[2][2] = -2/(far-near).
    float thickness = -0.5 * depth_diff * light_proj_from_view[2][2];

    return thickness;
}

float thickness2translucency(float thickness, float translucency_max_depth, vec3 N, vec3 L)
{
    return pow(1.0 - min(1.0, thickness / translucency_max_depth), 5.0) * max(0.0, -dot(N, L));
}

void main()
{
    float depth = texture(u_depth_texture, fs_in.texcoord).r;
    if (depth == 1.0) {
        discard;
    }

    // Fetch G-buffer info
    vec4 gbuffer0 = texture(u_base_color_gloss_texture, fs_in.texcoord);
    vec4 gbuffer1 = texture(u_normal_texture, fs_in.texcoord);
    vec4 gbuffer2 = texture(u_sss_emissive_texture, fs_in.texcoord);
    vec3 base_color = srgb2lin(gbuffer0.rgb);
    float gloss = gbuffer0.a;
    vec3 world_normal = 2.0 * gbuffer1.rgb - 1.0;
    float metalness = gbuffer1.a;
    float wrap = gbuffer2.g;
    vec3 world_pos = world_pos_from_depth(depth, fs_in.texcoord, u_scene.world_from_proj);

    // Calculate lighting vectors
    vec3 N = normalize(world_normal);
    vec3 L = normalize(-u_key_light_direction);
    vec3 V = normalize(u_scene.camera_world_pos.xyz - world_pos);
    vec3 H = normalize(L + V);
    vec3 R = normalize(reflect(-V, N));
    float N_dot_L = max(0.0, dot(N, L));

    // Determine occlusion and visibility
    float ao = texture(u_ssao_texture, fs_in.texcoord).r;
    vec3 diffuse_occlusion = ao_approx_bounce(ao, base_color);
    float horizon_specular_occlusion = gbuffer2.a;
    float specular_occlusion = ao_approx_specular_occlusion(ao, N, V, gloss);
    specular_occlusion = min(specular_occlusion, horizon_specular_occlusion);

    float visibility = 1.0;
    if (u_shadows_enabled) {
        vec2 rnd = texelFetch(u_blue_noise_texture, ivec2(gl_FragCoord.xy) % 64, 0).ba;
        rnd.x = hash(rnd.x + float(u_frame_index % 1000));
        rnd.y = fract(rnd.y + sqrt(3.0) * (u_frame_index % 1000));

        float normal_bias = u_shadow_normal_bias * (1.0 - N_dot_L);
        float depth_bias = mix(u_shadow_slope_bias.y, u_shadow_slope_bias.x, N_dot_L);
        visibility = sample_shadow_map(u_shadow_map_texture, world_pos + normal_bias * N,
                                       u_key_light_proj_from_world, depth_bias, u_pdf_radius,
                                       u_pdf_num_samples, rnd);
    }

    // Estimate thick-object translucency from key light shadow map
    float translucency = 0.0;
    if (u_shadows_enabled && u_translucency_enabled) {
        float thickness = estimate_thickness(u_shadow_map_texture, world_pos, N,
                                             u_key_light_proj_from_world,
                                             u_key_light_proj_from_view);
        translucency = thickness2translucency(thickness, u_translucency_max_depth, N, L);
    }

    // Calculate diffuse and specular lighting
    vec3 specular_color = mix(vec3(0.03), base_color, metalness);
    float specular_power = gloss_to_specular_power(gloss);
    float roughness = gloss_to_roughness(gloss);
    float max_mip_level = log2(textureSize(u_cubemap_texture, 0).x);
    vec3 F0 = specular_color;
    vec3 F = fresnel_schlick_gloss(F0, V, N, gloss);

    vec3 output_color_diffuse = ((1.0 - F0) * visibility * diffuse_wrap(N, L, wrap) *
                                 u_key_light_intensity);
    output_color_diffuse += 0.2 * srgb2lin(u_scatter_color) * translucency * u_key_light_intensity;
    output_color_diffuse += ((1.0 - F) * diffuse_occlusion * u_sky_light_intensity *
                             textureLod(u_cubemap_texture, N, max_mip_level).rgb);
    output_color_diffuse *= 1.0 - metalness;
    output_color_diffuse *= u_exposure;

    vec3 output_color_specular = (visibility * fresnel_schlick(specular_color, L, H) *
                                  specular_D_ggx(N, H, roughness) * u_key_light_intensity *
                                  N_dot_L);
    output_color_specular += (F * specular_occlusion * u_sky_light_intensity *
                              sample_radiance_envmap(u_cubemap_texture, R, specular_power));
    output_color_specular *= u_exposure;

    rt_diffuse = vec4(output_color_diffuse, 1.0);
    rt_specular = vec4(output_color_specular, 1.0);
}
