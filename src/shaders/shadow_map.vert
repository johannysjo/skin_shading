#version 450

layout(location = 0) uniform mat4 u_proj_from_model;

layout(location = 0) in vec4 a_model_pos;

void main()
{
    gl_Position = u_proj_from_model * a_model_pos;
}
