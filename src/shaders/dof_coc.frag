#version 450

layout(binding = 0) uniform sampler2D u_depth_texture;

layout(location = 0) uniform float u_focus_distance;
layout(location = 1) uniform float u_z_near;
layout(location = 2) uniform float u_z_far;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

float linearize_depth(float depth, float z_near, float z_far)
{
    return 2.0 * z_far * z_near / (z_far + z_near - (2.0 * depth - 1.0) * (z_far - z_near));
}

float circle_of_confusion(float linear_depth, float focus_distance)
{
    return clamp(3.0 * (linear_depth - focus_distance) / focus_distance, -1.0, 1.0);
}

void main()
{
    float depth = texture(u_depth_texture, fs_in.texcoord).r;
    float linear_depth = linearize_depth(depth, u_z_near, u_z_far);
    float coc = circle_of_confusion(linear_depth, u_focus_distance);

    frag_color = vec4(coc, 0.0, 0.0, 0.0);
}
