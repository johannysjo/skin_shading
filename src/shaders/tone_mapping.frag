#version 450

layout(binding = 0) uniform sampler2D u_texture;

layout(location = 0) uniform uint u_tone_mapping_operator;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

#define ACES 0
#define GAMMA_ONLY 1
#define NONE 2

vec3 aces(vec3 x)
{
    float a = 2.51;
    float b = 0.03;
    float c = 2.43;
    float d = 0.59;
    float e = 0.14;
    return clamp((x * (a * x + b)) / (x * (c * x + d) + e), 0.0, 1.0);
}

vec3 lin2srgb(vec3 color)
{
    return pow(color, vec3(0.454));
}

// Screen-space dither function from Alex Vlacho's "Advanced VR Rendering"
// presentation, GDC 2015.
vec3 screen_space_dither(vec2 screen_pos)
{
    vec3 dither = vec3(dot(vec2(171.0, 231.0), screen_pos));
    dither = fract(dither / vec3(103.0, 71.0, 97.0)) - 0.5;
	return (dither / 255.0) * 0.375;
}

void main()
{
    vec4 color = texture(u_texture, fs_in.texcoord);

    if (u_tone_mapping_operator == ACES) {
        color.rgb = aces(color.rgb);
        color.rgb = lin2srgb(color.rgb);
    }
    else if (u_tone_mapping_operator == GAMMA_ONLY) {
        color.rgb = lin2srgb(color.rgb);
    }
    else if (u_tone_mapping_operator == NONE) {
        // no tone mapping
    }

    color.rgb += screen_space_dither(gl_FragCoord.xy);

    frag_color = color;
}
