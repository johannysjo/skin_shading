#version 450

layout(binding = 0) uniform sampler2D u_texture;

layout(location = 0) uniform vec2 u_direction = vec2(1.0, 0.0);

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

void main()
{
    // This filter kernel utilizes linear interpolation to implement a 9-tap separable
    // Gaussian filter with only 5 texture fetches, as proposed by Daniel Rakos on
    // http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling
    float offsets[5] = float[](-3.23076, -1.38461, 0.0, 1.38461, 3.23076);
    float weights[5] = float[](0.07027, 0.31621, 0.22702, 0.31621, 0.07027);
    vec2 scaling = u_direction / vec2(textureSize(u_texture, 0));
    vec4 color = vec4(0.0);
    for (int i = 0; i < 5; ++i) {
        vec2 offset = scaling * offsets[i];
        color += texture(u_texture, fs_in.texcoord + offset) * weights[i];
    }

    frag_color = color;
}
