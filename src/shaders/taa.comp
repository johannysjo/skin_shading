#version 450

layout(local_size_x = 8, local_size_y = 8) in;

layout(binding = 0) uniform sampler2D u_current;
layout(binding = 1, rgba16f) uniform restrict image2D u_history;

vec3 tonemap(vec3 color)
{
    return color / (1.0 + color);
}

vec3 tonemap_inv(vec3 color)
{
    return color / (1.0 - 0.9999 * color);
}

// Temporal anti-aliasing (TAA) with color variance clipping.
// Reference: Marco Salvi, An Excursion in Temporal Supersampling, GDC 2016.
vec3 taa(ivec2 texcoord, float alpha)
{
    // Calculate the first and second raw moments of the local color
    // distribution in the current frame
    vec3 m1 = vec3(0.0);
    vec3 m2 = vec3(0.0);
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            vec3 color = tonemap(texelFetch(u_current, texcoord + ivec2(i, j), 0).rgb);
            m1 += color;
            m2 += color * color;
        }
    }

    // Construct color AABB from raw moments
    vec3 mean_value = m1 / 9.0;
    vec3 sigma = sqrt(abs(m2 / 9.0 - mean_value * mean_value));
    vec3 aabb_min_corner = mean_value - sigma;
    vec3 aabb_max_corner = mean_value + sigma;

    // Clip previous accumulated fragment color against AABB
    vec3 previous_color = tonemap(imageLoad(u_history, texcoord).rgb);
    vec3 clipped_color = clamp(previous_color, aabb_min_corner, aabb_max_corner);

    // Use an exponetial moving average filter to blend the current sample color
    // with the clipped color
    vec3 current_color = tonemap(texelFetch(u_current, texcoord, 0).rgb);
    vec3 output_color = tonemap_inv(mix(clipped_color, current_color, alpha));

    return output_color;
}

void main()
{
    ivec2 texcoord = ivec2(gl_GlobalInvocationID.xy);
    vec3 output_color = taa(texcoord, 0.1);

    imageStore(u_history, texcoord, vec4(output_color, 1.0));
}
