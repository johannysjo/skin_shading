#version 450

layout(binding = 0) uniform sampler2D u_color_texture;

layout(location = 0) uniform float u_sharpness_knob = 0.0; // min 0.0, max 1.0

layout(location = 0) out vec4 frag_color;

vec3 srgb2lin(vec3 color)
{
    return color * color;
}

vec3 lin2srgb(vec3 color)
{
    return sqrt(color);
}

// Contrast Adaptive Sharpening (CAS)
// Reference: Lou Kramer, FidelityFX CAS, AMD Developer Day 2019,
// https://gpuopen.com/wp-content/uploads/2019/07/FidelityFX-CAS.pptx
vec3 cas(sampler2D tex, ivec2 texcoord, float sharpness_knob)
{
    vec3 a = srgb2lin(texelFetch(tex, texcoord + ivec2(0.0, -1.0), 0).rgb);
    vec3 b = srgb2lin(texelFetch(tex, texcoord + ivec2(-1.0, 0.0), 0).rgb);
    vec3 c = srgb2lin(texelFetch(tex, texcoord + ivec2(0.0, 0.0), 0).rgb);
    vec3 d = srgb2lin(texelFetch(tex, texcoord + ivec2(1.0, 0.0), 0).rgb);
    vec3 e = srgb2lin(texelFetch(tex, texcoord + ivec2(0.0, 1.0), 0).rgb);

    float min_g = min(a.g, min(b.g, min(c.g, min(d.g, e.g))));
    float max_g = max(a.g, max(b.g, max(c.g, max(d.g, e.g))));
    float sharpening_amount = sqrt(min(1.0 - max_g, min_g) / max_g);
    float w = sharpening_amount * mix(-0.125, -0.2, sharpness_knob);

    return lin2srgb((w * (a + b + d + e) + c) / (4.0 * w + 1.0));
}

void main()
{
    ivec2 texcoord = ivec2(gl_FragCoord.xy);
    frag_color = vec4(cas(u_color_texture, texcoord, u_sharpness_knob), 1.0);
}
