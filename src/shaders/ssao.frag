#version 450

layout(binding = 0) uniform sampler2D u_depth_texture;
layout(binding = 1) uniform sampler2D u_half_res_depth_texture;
layout(binding = 2) uniform sampler2D u_world_normal_texture;
layout(binding = 3) uniform sampler2D u_blue_noise_texture;

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
} u_scene;

layout(location = 0) uniform uint u_frame_index;
layout(location = 1) uniform float u_radius; // in world units
layout(location = 2) uniform float u_sphere_offset;
layout(location = 3) uniform uint u_num_samples;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

#define PI 3.14159265358

vec2 sample_vogel_disk(uint i, uint num_samples, vec2 rnd)
{
    float r = sqrt((float(i) + rnd.x) / float(num_samples));
    float golden_angle = 2.4;
    float phi = float(i) * golden_angle + 2.0 * PI * rnd.y;
    float x = r * cos(phi);
    float y = r * sin(phi);

    return vec2(x, y);
}

float hash(float seed)
{
    return fract(sin(seed * 12.9899) * 43758.5453); 
}

vec3 view_pos_from_depth(float depth, vec2 texcoord)
{
    vec4 ndc_pos = vec4(2.0 * texcoord - 1.0, 2.0 * depth - 1.0, 1.0);
    vec4 view_pos = u_scene.view_from_proj * ndc_pos;
    view_pos.xyz /= view_pos.w;

    return view_pos.xyz;
}

vec2 texcoord_from_view_pos(vec3 view_pos)
{
    vec4 clip_pos = u_scene.proj_from_view * vec4(view_pos, 1.0);
    vec2 texcoord = 0.5 * (clip_pos.xy / clip_pos.w) + 0.5;

    return texcoord;
}

// Computes volumetric ambient occlusion from a depth buffer by taking line
// samples in a tangent sphere. The sample points are selected from a 2D
// low-discrepancy sequence (Vogel disk) that is randomly rotated and jittered
// to reduce banding.
//
// Reference: Szirmay-Kalos et al. Volumetric Ambient Occlusion for Real-Time
// Rendering and Games, IEEE Computer Graphics and Applications, 2010.
float compute_vao(vec2 texcoord, float center_depth, float radius, float sphere_offset,
                  uint num_samples, vec2 rnd)
{
    vec3 center_view_pos = view_pos_from_depth(center_depth, texcoord);
    vec3 world_normal = normalize(2.0 * texture(u_world_normal_texture, texcoord).rgb - 1.0);
    vec3 view_normal = normalize(mat3(u_scene.view_from_world) * world_normal);
    center_view_pos += sphere_offset * radius * view_normal;

    float radius_sq = radius * radius;
    float occlusion = 0.0;
    float max_line_length_sum = 0.0;
    for (uint i = 0; i < num_samples; ++i) {
        vec2 view_offset = radius * sample_vogel_disk(i, num_samples, rnd);
        vec2 texcoord_i = texcoord_from_view_pos(center_view_pos + vec3(view_offset, 0.0));
        float depth = texture(u_half_res_depth_texture, texcoord_i).r;
        vec3 view_pos = view_pos_from_depth(depth, texcoord_i);

        float max_line_length = 2.0 * sqrt(max(0.0, radius_sq - dot(view_offset, view_offset)));
        float z_sphere_entry = center_view_pos.z + 0.5 * max_line_length;
        float z_occluder = view_pos.z;
        float z_diff = z_sphere_entry - z_occluder;
        float unoccluded_line_length = clamp(z_diff, 0.0, max_line_length);
        float falloff = 1.0 - clamp(-z_diff / (3.0 * radius), 0.0, 1.0);

        occlusion += unoccluded_line_length;
        max_line_length_sum += falloff * max_line_length;
    }
    occlusion /= max(max_line_length_sum, 1e-3);

    return occlusion;
}

void main()
{
    float center_depth = texture(u_depth_texture, fs_in.texcoord).r;
    float occlusion = 1.0;
    if (center_depth < 1.0) {
        vec2 rnd = texelFetch(u_blue_noise_texture, ivec2(gl_FragCoord.xy) % 64, 0).rg;
        rnd.x = hash(rnd.x + float(u_frame_index % 1000));
        rnd.y = fract(rnd.y + sqrt(3.0) * float(u_frame_index % 1000));

        occlusion = compute_vao(fs_in.texcoord, center_depth, u_radius, u_sphere_offset,
                                u_num_samples, rnd);
    }

    frag_color = vec4(occlusion, 0.0, 0.0, 0.0);
}
