#version 450

layout(binding = 0) uniform sampler2D u_diffuse_texture;
layout(binding = 1) uniform sampler2D u_depth_texture;
layout(binding = 2) uniform sampler2D u_blue_noise_texture;
layout(binding = 3) uniform sampler2D u_base_color_texture;
layout(binding = 4) uniform sampler2D u_specular_texture;
layout(binding = 5) uniform sampler2D u_sss_emissive_texture;

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
} u_scene;

layout(location = 0) uniform uint u_frame_index;
layout(location = 1) uniform float u_scale;
layout(location = 2) uniform uint u_num_samples;
layout(location = 3) uniform bool u_sss_enabled;
layout(location = 4) uniform vec3 u_sigma_far_mm;
layout(location = 5) uniform float u_w;
layout(location = 6) uniform float u_exposure;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

#define PI 3.14159265358

vec3 srgb2lin(vec3 color)
{
    return pow(color, vec3(2.2));
}

float decode_emissive(float emissive_ldr)
{
    float emissive_hdr = emissive_ldr / max(1e-6, 1.0 - emissive_ldr);
    return emissive_hdr;
}

vec2 sample_non_uniform_vogel_disk(uint i, uint num_samples, vec2 rnd)
{
    float r = (float(i) + rnd.x) / float(num_samples);
    float golden_angle = 2.4;
    float phi = float(i) * golden_angle + 2.0 * PI * rnd.y;
    float x = r * cos(phi);
    float y = r * sin(phi);

    return vec2(x, y);
}

float hash(float seed)
{
    return fract(sin(seed * 12.9899) * 43758.5453);
}

vec3 view_pos_from_depth(float depth, vec2 texcoord)
{
    vec4 ndc_pos = vec4(2.0 * texcoord - 1.0, 2.0 * depth - 1.0, 1.0);
    vec4 view_pos = u_scene.view_from_proj * ndc_pos;
    view_pos.xyz /= view_pos.w;

    return view_pos.xyz;
}

vec2 texcoord_from_view_pos(vec3 view_pos)
{
    vec4 clip_pos = u_scene.proj_from_view * vec4(view_pos, 1.0);
    vec2 texcoord = 0.5 * (clip_pos.xy / clip_pos.w) + 0.5;

    return texcoord;
}

vec3 compute_sss(vec2 texcoord, float center_depth, vec3 center_diffuse_color, vec3 sigma_far_mm,
                 float w, uint num_samples, uint frame_index, vec2 rnd)
{
    vec3 center_view_pos = view_pos_from_depth(center_depth, texcoord);

    vec3 variance_mm = sigma_far_mm * sigma_far_mm;
    float radius_m = 1e-3 * 3.0 * max(sigma_far_mm.r, max(sigma_far_mm.g, sigma_far_mm.b));
    vec3 output_color = w * center_diffuse_color;
    vec3 weight_sum = vec3(w);
    for (uint i = 0; i < num_samples; ++i) {
        vec2 view_offset = radius_m * sample_non_uniform_vogel_disk(i, num_samples, rnd);
        vec2 texcoord_i = texcoord_from_view_pos(center_view_pos + vec3(view_offset, 0.0));
        float depth = texture(u_depth_texture, texcoord_i).r;
        vec3 view_pos = view_pos_from_depth(depth, texcoord_i);
        vec3 diffuse_color = texture(u_diffuse_texture, texcoord_i).rgb;
        vec3 view_pos_diff = 1e3 * (view_pos - center_view_pos);

        // NOTE: the area weight is required since we use a non-uniform vogel disk sampling
        // pattern that distributes more samples towards the center of the disk
        float near_spike_weight = 1.0 - w;
        float area_weight = 2.0 * float(i) + 1.0;
        vec3 gaussian_weight = exp(-0.5 * dot(view_pos_diff, view_pos_diff) / variance_mm);
        vec3 weight = near_spike_weight * area_weight * gaussian_weight;

        output_color += weight * diffuse_color;
        weight_sum += weight;
    }
    output_color /= weight_sum;

    return output_color;
}

void main()
{
    float depth = texture(u_depth_texture, fs_in.texcoord).r;
    if (depth == 1.0) {
        discard;
    }

    vec3 diffuse_color = texture(u_diffuse_texture, fs_in.texcoord).rgb;
    vec3 base_color = srgb2lin(texture(u_base_color_texture, fs_in.texcoord).rgb);
    vec3 specular_color = texture(u_specular_texture, fs_in.texcoord).rgb;
    float emissive = decode_emissive(texture(u_sss_emissive_texture, fs_in.texcoord).b);
    emissive *= u_exposure;

    if (u_sss_enabled) {
        vec2 rnd = texelFetch(u_blue_noise_texture, ivec2(gl_FragCoord.xy) % 64, 0).rg;
        rnd.r = hash(rnd.r + float(u_frame_index % 1000));
        rnd.g = fract(rnd.g + sqrt(3.0) * float(u_frame_index % 1000));
        diffuse_color = compute_sss(fs_in.texcoord, depth, diffuse_color, u_scale * u_sigma_far_mm,
                                    max(1e-3, u_w), u_num_samples, u_frame_index, rnd);
    }

    frag_color.rgb = base_color * (emissive + diffuse_color) + specular_color;
    frag_color.a = 1.0;
}
