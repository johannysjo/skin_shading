#version 450

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
    vec4 camera_world_pos;
} u_scene;

layout(location = 0) uniform mat4 u_world_from_model;

layout(location = 0) in vec4 a_model_pos;
layout(location = 1) in vec3 a_model_normal;
layout(location = 2) in vec2 a_texcoord;

out VS_OUT {
    vec3 world_pos;
    vec3 world_normal;
    vec2 texcoord;
} vs_out;

void main()
{
    vs_out.world_pos = (u_world_from_model * a_model_pos).xyz;
    vs_out.world_normal = mat3(u_world_from_model) * a_model_normal;
    vs_out.texcoord = a_texcoord;
    gl_Position = u_scene.proj_from_world * u_world_from_model * a_model_pos;
}
