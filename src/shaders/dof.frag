#version 450

layout(binding = 0) uniform sampler2D u_color_texture;
layout(binding = 1) uniform sampler2D u_coc_texture;
layout(binding = 2) uniform sampler2D u_blue_noise_texture;

layout(location = 0) uniform uint u_frame_index;
layout(location = 1) uniform float u_radius_pixels;
layout(location = 2) uniform uint u_num_samples;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

#define PI 3.14159265358

float hash(float seed)
{
    return fract(sin(seed * 12.9899) * 43758.5453);
}

vec2 sample_non_uniform_vogel_disk(uint i, uint num_samples, vec2 rnd)
{
    float r = ((float(i) + rnd.x) / float(num_samples));
    float golden_angle = 2.4;
    float phi = float(i) * golden_angle + 2.0 * PI * rnd.y;
    float x = r * cos(phi);
    float y = r * sin(phi);

    return vec2(x, y);
}

vec4 compute_dof(vec2 texcoord, sampler2D color_texture, sampler2D coc_texture,
                 float radius_pixels, uint num_samples, vec2 rnd)
{
    vec2 texel_size = 1.0 / textureSize(color_texture, 0);
    vec3 center_color = texture(color_texture, texcoord).rgb;
    float center_coc = radius_pixels * texture(coc_texture, texcoord).r;
    float center_weight = 0.01;

    vec3 output_color = center_weight * center_color;
    float weight_sum = center_weight;
    for (uint i = 0; i < num_samples; ++i) {
        vec2 offset = radius_pixels * sample_non_uniform_vogel_disk(i, num_samples, rnd);
        vec2 texcoord_i = texcoord + texel_size * offset;
        vec3 color_i = texture(color_texture, texcoord_i).rgb;
        float coc_i = radius_pixels * texture(coc_texture, texcoord_i).r;
        if (coc_i > center_coc) {
            coc_i = sign(coc_i) * clamp(abs(coc_i), 0.0, 2.0 * abs(center_coc));
        }
        float weight = 2.0 * float(i) + 1.0;
        float dist = length((texcoord - texcoord_i) / texel_size);
        if (dist <= abs(coc_i)) {
            output_color += weight * color_i;
        }
        else {
            output_color += weight * output_color / weight_sum;
        }
        weight_sum += weight;
    }
    output_color /= weight_sum;

    return vec4(output_color, center_coc);
}

void main()
{
    vec2 rnd = texelFetch(u_blue_noise_texture, ivec2(gl_FragCoord.xy) % 64, 0).ba;
    rnd.x = hash(rnd.x + float(u_frame_index % 1000));
    rnd.y = fract(rnd.y + sqrt(3.0) * float(u_frame_index % 1000));
    vec4 color_coc = compute_dof(fs_in.texcoord, u_color_texture, u_coc_texture,
                                 u_radius_pixels, u_num_samples, rnd);

    frag_color = color_coc;
}
