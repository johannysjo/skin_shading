#version 450

layout(binding = 0) uniform sampler2D u_color_texture;

layout(location = 0) uniform float u_bloom_intensity;

layout(location = 0) out vec4 frag_color;

void main()
{
    // Downsample the input color texture to 1/16 of its original resolution by averaging
    // the texel values in a 4x4 neighborhood. The filter utilizes linear interpolation to
    // approximate a 16-tap box blur with only 4 texel fetches. Assumes that linear
    // interpolation is enabled and that the viewport resolution is set to (w/4, h/4),
    // where (w, h) is the input texture resolution.
    vec2 src_texel_size = 1.0 / textureSize(u_color_texture, 0);
    vec2 dst_texcoord = (floor(gl_FragCoord.xy) * 4.0 + 2.0) * src_texel_size;
    vec3 color =
        texture(u_color_texture, dst_texcoord + vec2(-1.0, -1.0) * src_texel_size).rgb +
        texture(u_color_texture, dst_texcoord + vec2( 1.0, -1.0) * src_texel_size).rgb +
        texture(u_color_texture, dst_texcoord + vec2(-1.0,  1.0) * src_texel_size).rgb +
        texture(u_color_texture, dst_texcoord + vec2( 1.0,  1.0) * src_texel_size).rgb;
    color *= 0.25;

    frag_color = vec4(u_bloom_intensity * color, 1.0);
}
