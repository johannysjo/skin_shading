#version 450

layout(binding = 0) uniform sampler2D u_base_color_texture;
layout(binding = 1) uniform sampler2D u_metalness_texture;
layout(binding = 2) uniform sampler2D u_bump_map_texture;
layout(binding = 3) uniform sampler2D u_sss_mask_texture;
layout(binding = 4) uniform sampler2D u_emissive_texture;

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
    vec4 camera_world_pos;
} u_scene;

layout(location = 1) uniform float u_gloss;
layout(location = 2) uniform float u_bump_scale;
layout(location = 3) uniform vec3 u_tint_color = vec3(1.0);
layout(location = 4) uniform float u_wrap;
layout(location = 5) uniform float u_emissive_strength;

in VS_OUT {
    vec3 world_pos;
    vec3 world_normal;
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 rt_buffer0;
layout(location = 1) out vec4 rt_buffer1;
layout(location = 2) out vec4 rt_buffer2;

vec3 srgb2lin(vec3 color)
{
    return color * color;
}

vec3 lin2srgb(vec3 color)
{
    return sqrt(color);
}

float encode_emissive(float emissive_hdr)
{
    float emissive_ldr = emissive_hdr / (1.0 + emissive_hdr);
    return emissive_ldr;
}

vec3 normalize_safe(vec3 v)
{
    float v_length = length(v);
    return v_length > 0.0 ? v / v_length : v;
}

// Constructs an orthonormal TBN basis from screen-space derivatives.
// Reference: http://www.thetenthplanet.de/archives/1180
mat3 construct_tbn_basis(vec3 N, vec3 position, vec2 texcoord)
{
    vec3 dpdx = dFdx(position);
    vec3 dpdy = dFdy(position);
    vec2 dtdx = dFdx(texcoord);
    vec2 dtdy = dFdy(texcoord);
    vec3 N_cross_dpdx = cross(N, dpdx);
    vec3 dpdy_cross_N = cross(dpdy, N);

    vec3 T = normalize_safe(dpdy_cross_N * dtdx.s + N_cross_dpdx * dtdy.s);
    vec3 B = normalize_safe(dpdy_cross_N * dtdy.t + N_cross_dpdx * dtdy.t);

    return mat3(T, B, N);
}

vec3 compute_gradient(sampler2D bump_map_texture, vec2 texcoord, vec2 delta, float bump_scale)
{
    vec3 gradient;
    gradient.x = (texture(bump_map_texture, texcoord - vec2(delta.x, 0.0)).r -
                  texture(bump_map_texture, texcoord + vec2(delta.x, 0.0)).r);
    gradient.y = (texture(bump_map_texture, texcoord - vec2(0.0, delta.y)).r -
                  texture(bump_map_texture, texcoord + vec2(0.0, delta.y)).r);
    gradient.z = 2.0 / bump_scale;

    return gradient;
}

void main()
{
    vec3 base_color = srgb2lin(texture(u_base_color_texture, fs_in.texcoord).rgb);
    base_color *= srgb2lin(u_tint_color);
    float metalness = texture(u_metalness_texture, fs_in.texcoord).r;
    float sss_mask = texture(u_sss_mask_texture, fs_in.texcoord).r;
    float emissive = u_emissive_strength * texture(u_emissive_texture, fs_in.texcoord).r;

    // Fetch the geometric normal
    vec3 N_geom = normalize_safe(fs_in.world_normal);

    // Apply bump mapping
    mat3 TBN = construct_tbn_basis(N_geom, fs_in.world_pos, fs_in.texcoord);
    vec2 delta = 1.0 / textureSize(u_bump_map_texture, 0);
    vec3 gradient = compute_gradient(u_bump_map_texture, fs_in.texcoord, delta, u_bump_scale);
    vec3 N_bump = normalize_safe(gradient);
    vec3 N = normalize(N_geom + TBN * N_bump);

    // Reflection vectors calculated from perturbed surface normals might end up pointing below the
    // surface horizon, which can lead to undesired specular ambient light leaking. To correct for
    // this, we calculate a horizon specular occlusion term as proposed by Jeff Russel on
    // https://marmosetco.tumblr.com/post/81245981087.
    vec3 V = normalize(u_scene.camera_world_pos.xyz - fs_in.world_pos);
    vec3 R = reflect(-V, N);
    float horizon_specular_occlusion = min(1.0, 1.0 + dot(R, N_geom));
    horizon_specular_occlusion *= horizon_specular_occlusion;

    // Perform Gram-Schmidt orthogonalization to ensure that the perturbed surface normal does not
    // point away from the camera
    float N_dot_V = dot(N, V);
    N = N_dot_V < 0.0 ? normalize(N - V * N_dot_V) : N;

    rt_buffer0 = vec4(lin2srgb(base_color), u_gloss);
    rt_buffer1 = vec4(0.5 * N + 0.5, metalness);
    rt_buffer2 = vec4(sss_mask, u_wrap, encode_emissive(emissive), horizon_specular_occlusion);
}
