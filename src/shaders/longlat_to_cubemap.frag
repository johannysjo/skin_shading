#version 450

layout(binding = 0) uniform sampler2D u_envmap_texture;

layout(location = 0) uniform mat4 u_rotation;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

vec4 sample_longlat_envmap(sampler2D envmap_texture, vec3 ray_dir)
{
    // Convert the 3D ray direction to an equirectangular 2D texture coordinate (without using
    // costly inverse trigonometric functions.)
    // Reference: OpenGL SuperBible, 6th edition, Listing 12.11.
    vec2 texcoord;
    texcoord.s = 0.75 - 0.25 * sign(ray_dir.z) * (1.0 - normalize(ray_dir.xz).x);
    texcoord.t = 0.5 - 0.5 * ray_dir.y;

    return texture(envmap_texture, texcoord);
}

void main()
{
    vec2 uv = vec2(fs_in.texcoord.x, 1.0 - fs_in.texcoord.y);
    vec3 ray_dir = normalize((u_rotation * vec4(2.0 * uv - 1.0, 1.0, 1.0)).xyz);
    frag_color = sample_longlat_envmap(u_envmap_texture, ray_dir);
}
