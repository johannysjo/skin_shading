#version 450

layout(binding = 0) uniform sampler2D u_depth_texture;

layout(location = 0) out vec4 frag_color;

void main()
{
    ivec2 texcoord_half_res = ivec2(gl_FragCoord.xy);
    ivec2 texcoord = 2 * texcoord_half_res;
    float min_depth = texelFetch(u_depth_texture, texcoord, 0).r;
    min_depth = min(min_depth, texelFetch(u_depth_texture, texcoord + ivec2(1, 0), 0).r);
    min_depth = min(min_depth, texelFetch(u_depth_texture, texcoord + ivec2(0, 1), 0).r);
    min_depth = min(min_depth, texelFetch(u_depth_texture, texcoord + ivec2(1, 1), 0).r);

    frag_color = vec4(min_depth, 0.0, 0.0, 0.0);
}
