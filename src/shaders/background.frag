#version 450

layout(binding = 0) uniform sampler2D u_depth_texture;
layout(binding = 1) uniform samplerCube u_cubemap_texture;

layout(std140, binding = 0) uniform SceneUBO {
    mat4 view_from_world;
    mat4 proj_from_view;
    mat4 proj_from_world;
    mat4 world_from_view;
    mat4 view_from_proj;
    mat4 world_from_proj;
    vec4 camera_world_pos;
} u_scene;

layout(location = 0) uniform float u_intensity;
layout(location = 1) uniform float u_exposure;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

vec3 get_camera_ray_direction(mat4 view_from_world, mat4 proj_from_view, vec2 uv)
{
	vec3 f = -vec3(view_from_world[0][2], view_from_world[1][2], view_from_world[2][2]);
	vec3 s = vec3(view_from_world[0][0], view_from_world[1][0], view_from_world[2][0]);
    vec3 u = vec3(view_from_world[0][1], view_from_world[1][1], view_from_world[2][1]);
    float half_height = 1.0 / proj_from_view[1][1];
    float half_width = 1.0 / proj_from_view[0][0];
    float x = 2.0 * uv.x - 1.0;
    float y = 2.0 * uv.y - 1.0;

	return normalize(f + x * half_width * s + y * half_height * u);
}

vec4 sample_cubemap(samplerCube cubemap_texture, vec3 ray_dir, float mip_level)
{
    float texture_size = textureSize(cubemap_texture, int(mip_level)).x;
    vec3 abs_ray_dir = abs(ray_dir);
    vec3 tc = ray_dir / max(abs_ray_dir.x, max(abs_ray_dir.y, abs_ray_dir.z));
    tc = texture_size * (0.5 * tc + 0.5) + 0.5;
    tc = floor(tc) + smoothstep(0.0, 1.0, fract(tc));
    tc = 2.0 * (tc - 0.5) / texture_size - 1.0;

    return textureLod(cubemap_texture, tc, mip_level);
}

void main()
{
    float depth = texture(u_depth_texture, fs_in.texcoord).r;
    if (depth < 1.0) {
        discard;
    }

    vec3 ray_dir = get_camera_ray_direction(u_scene.view_from_world, u_scene.proj_from_view,
                                            fs_in.texcoord);
    vec3 color = u_exposure * u_intensity * sample_cubemap(u_cubemap_texture, ray_dir, 4.0).rgb;

    frag_color = vec4(color, 1.0);
}
