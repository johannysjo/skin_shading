#pragma once

#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace gfx { // forward declarations
struct Texture2D;
struct TextureCubemap;
} // namespace gfx

namespace utils {

struct Mesh {
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texcoords;
};

struct MeshVAO {
    GLuint vao;
    GLuint vertex_buffer;
    GLuint normal_buffer;
    GLuint texcoord_buffer;
    uint32_t num_vertices;
};

struct BoundingBox {
    glm::vec3 min_corner;
    glm::vec3 max_corner;
};

struct Camera {
    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    float fovy;
    float aspect;
    float z_near;
    float z_far;
};

struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 color;
    float intensity;
};

MeshVAO create_mesh_vao(const Mesh &mesh);

BoundingBox compute_bbox(const std::vector<float> &vertices);

void fit_camera_to_bbox(Camera &camera, const BoundingBox &bbox);

float get_auto_focus_distance(const Camera &camera, const BoundingBox &bbox);

glm::mat4 get_light_view_from_world_matrix(const DirectionalLight &light, const BoundingBox &bbox);

glm::mat4 get_light_proj_from_view_matrix(const BoundingBox &bbox);

glm::mat4 get_taa_offset_matrix(uint32_t width, uint32_t height, uint32_t frame_index,
                                bool taa_enabled);

void create_solid_r8_texture(uint32_t width, uint32_t height, uint8_t value,
                             gfx::Texture2D &texture);

void generate_diffusion_profile_texture(gfx::Texture2D &texture, const glm::vec3 &sss_sigma_far,
                                        float sss_w);

void generate_mip_cubemap(const GLuint program, const gfx::Texture2D &longlat_envmap,
                          gfx::TextureCubemap &cubemap);

} // namespace utils
