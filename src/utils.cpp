#include "utils.h"

#include "gfx.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec3.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace {

GLuint create_buffer(const std::vector<float> &data)
{
    assert(!data.empty());

    GLuint buffer;
    glCreateBuffers(1, &buffer);
    const size_t size_in_bytes = data.size() * sizeof(data[0]);
    glNamedBufferStorage(buffer, size_in_bytes, data.data(), GL_DYNAMIC_STORAGE_BIT);

    return buffer;
}

// The R2 low-discrepancy sequence, generates evenly distributed points in [0,1)^2
// Reference: http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/
glm::vec2 ldseq_r2(const float n)
{
    const float phi = 1.324717957244746f;
    const float x = std::fmod(n / phi, 1.0f);
    const float y = std::fmod(n / (phi * phi), 1.0f);

    return {x, y};
}

float gaussian(const float sigma, const float x)
{
    return std::exp(-0.5f * (x * x) / (sigma * sigma));
}

} // namespace

namespace utils {

MeshVAO create_mesh_vao(const Mesh &mesh)
{
    assert(!mesh.vertices.empty());
    assert(!mesh.normals.empty());
    assert(!mesh.texcoords.empty());

    GLuint vao;
    glCreateVertexArrays(1, &vao);

    GLuint vertex_buffer = create_buffer(mesh.vertices);
    const GLuint vertex_binding_index = 0;
    glVertexArrayVertexBuffer(vao, vertex_binding_index, vertex_buffer, 0, 3 * sizeof(float));
    glEnableVertexArrayAttrib(vao, vertex_binding_index);
    glVertexArrayAttribFormat(vao, vertex_binding_index, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, vertex_binding_index, vertex_binding_index);

    GLuint normal_buffer = create_buffer(mesh.normals);
    const GLuint normal_binding_index = 1;
    glVertexArrayVertexBuffer(vao, normal_binding_index, normal_buffer, 0, 3 * sizeof(float));
    glEnableVertexArrayAttrib(vao, normal_binding_index);
    glVertexArrayAttribFormat(vao, normal_binding_index, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, normal_binding_index, normal_binding_index);

    GLuint texcoord_buffer = create_buffer(mesh.texcoords);
    const GLuint texcoord_binding_index = 2;
    glVertexArrayVertexBuffer(vao, texcoord_binding_index, texcoord_buffer, 0, 2 * sizeof(float));
    glEnableVertexArrayAttrib(vao, texcoord_binding_index);
    glVertexArrayAttribFormat(vao, texcoord_binding_index, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, texcoord_binding_index, texcoord_binding_index);

    MeshVAO mesh_vao{};
    mesh_vao.vao = vao;
    mesh_vao.vertex_buffer = vertex_buffer;
    mesh_vao.normal_buffer = normal_buffer;
    mesh_vao.texcoord_buffer = texcoord_buffer;
    mesh_vao.num_vertices = uint32_t(mesh.vertices.size() / 3);

    return mesh_vao;
}

BoundingBox compute_bbox(const std::vector<float> &vertices)
{
    assert(vertices.size() >= 3);
    assert(vertices.size() % 3 == 0);

    BoundingBox bbox = {glm::vec3(vertices[0], vertices[1], vertices[2]),
                        glm::vec3(vertices[0], vertices[1], vertices[2])};

    const size_t num_vertices = vertices.size();
    for (size_t i = 3; i < num_vertices; i += 3) {
        const float x = vertices[i];
        const float y = vertices[i + 1];
        const float z = vertices[i + 2];

        bbox.min_corner[0] = std::fmin(x, bbox.min_corner[0]);
        bbox.min_corner[1] = std::fmin(y, bbox.min_corner[1]);
        bbox.min_corner[2] = std::fmin(z, bbox.min_corner[2]);

        bbox.max_corner[0] = std::fmax(x, bbox.max_corner[0]);
        bbox.max_corner[1] = std::fmax(y, bbox.max_corner[1]);
        bbox.max_corner[2] = std::fmax(z, bbox.max_corner[2]);
    }

    return bbox;
}

void fit_camera_to_bbox(Camera &camera, const BoundingBox &bbox)
{
    assert(camera.fovy > 0.0f);

    const glm::vec3 bsphere_center = 0.5f * (bbox.min_corner + bbox.max_corner);
    const float bsphere_radius = 0.5f * glm::length(bbox.max_corner - bbox.min_corner);
    const float margin = bsphere_radius > 0.0f ? 0.01f * bsphere_radius : 0.01f;
    const float eye_to_bsphere_center_distance =
        camera.z_near + margin + bsphere_radius / std::tan(0.5f * camera.fovy);

    const glm::vec3 view_dir = glm::normalize(camera.center - camera.eye);
    assert(std::abs(glm::dot(view_dir, camera.up)) < 1.0f);

    camera.eye = bsphere_center - eye_to_bsphere_center_distance * view_dir;
    camera.center = bsphere_center;
    camera.z_far = 2.0f * eye_to_bsphere_center_distance;
}

float get_auto_focus_distance(const Camera &camera, const BoundingBox &bbox)
{
    const glm::vec3 bsphere_center = 0.5f * (bbox.min_corner + bbox.max_corner);
    const float bsphere_radius = 0.5f * glm::length(bbox.max_corner - bbox.min_corner);

    // Project the bounding sphere center onto the camera view direction vector and calculate the
    // distance between the camera and the projected bpshere center.
    const glm::vec3 view_dir = glm::normalize(camera.center - camera.eye);
    const glm::vec3 eye_to_bsphere_center = bsphere_center - camera.eye;
    const float eye_to_projected_bsphere_center_dist =
        std::fmax(0.0f, glm::dot(view_dir, eye_to_bsphere_center));

    // Place the focus plane roughly two thirds into the projected bounding sphere. Totally
    // arbitrary, but seems to work well...
    const float focus_distance =
        std::fmax(0.0f, eye_to_projected_bsphere_center_dist - 0.35 * bsphere_radius);

    return focus_distance;
}

glm::mat4 get_light_view_from_world_matrix(const DirectionalLight &light, const BoundingBox &bbox)
{
    const glm::vec3 bsphere_center = 0.5f * (bbox.min_corner + bbox.max_corner);
    const float bsphere_radius = 0.5f * glm::length(bbox.max_corner - bbox.min_corner);
    const float margin = bsphere_radius > 0.0f ? 0.01f * bsphere_radius : 0.01f;

    const glm::vec3 eye =
        bsphere_center - (bsphere_radius + margin) * glm::normalize(light.direction);
    const glm::vec3 center = bsphere_center;
    const glm::vec3 up(0.0f, 1.0f, 0.0f);
    const glm::mat4 view_from_world = glm::lookAt(eye, center, up);

    return view_from_world;
}

glm::mat4 get_light_proj_from_view_matrix(const BoundingBox &bbox)
{
    const float bsphere_radius = 0.5f * glm::length(bbox.max_corner - bbox.min_corner);
    const float margin = bsphere_radius > 0.0f ? 0.01f * bsphere_radius : 0.01f;

    const float left = -bsphere_radius - margin;
    const float right = bsphere_radius + margin;
    const float bottom = -bsphere_radius - margin;
    const float top = bsphere_radius + margin;
    const float z_near = margin;
    const float z_far = 2.0f * (bsphere_radius + margin);
    const glm::mat4 proj_from_view = glm::ortho(left, right, bottom, top, z_near, z_far);

    return proj_from_view;
}

glm::mat4 get_taa_offset_matrix(const uint32_t width, const uint32_t height,
                                const uint32_t frame_index, const bool taa_enabled)
{
    if (!taa_enabled)
        return glm::mat4(1.0f);

    const glm::vec2 texel_size = {1.0f / width, 1.0f / height};
    const glm::vec2 rnd = ldseq_r2(float(frame_index % 1000));
    const glm::vec2 offset = texel_size * (2.0f * rnd - 1.0f);

    return glm::translate(glm::mat4(1.0f), glm::vec3(offset[0], offset[1], 0.0f));
}

void create_solid_r8_texture(const uint32_t width, const uint32_t height, const uint8_t value,
                             gfx::Texture2D &texture)
{
    assert(width > 0);
    assert(height > 0);

    const std::vector<uint8_t> data(width * height, value);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_MIRRORED_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data.data());
}

// Generates a diffusion profile texture where the profile is composed of a spike and three
// Gaussians (one per channel). The sigma_far color controls the variance/width of the Gaussians,
// and w is a value between 0 and 1 controlling the strength of the spike.
//
// Reference: M.S. Mikkelsen, Skin Rendering by Pseudo–Separable Cross Bilateral Filtering, 2010.
void generate_diffusion_profile_texture(gfx::Texture2D &texture, const glm::vec3 &sss_sigma_far,
                                        const float sss_w)
{
    assert(sss_w >= 0.0f);

    glDeleteTextures(1, &texture.texture);

    const uint32_t width = 256;
    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = 1;
    texture.storage = {GL_RGBA32F, GL_RGBA, GL_FLOAT, 4};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);

    const float irradiance = 1.0f;
    std::vector<float> diffusion_profile(width * 4, 0.0f);
    for (uint32_t i = 0; i < width; ++i) {
        const float r = 4.0f * float(i) / width;
        const float spike = r > 0.0f ? 0.0f : 1.0;
        diffusion_profile[4 * i + 0] =
            irradiance * (sss_w * spike + (1.0f - sss_w) * gaussian(sss_sigma_far[0], r));
        diffusion_profile[4 * i + 1] =
            irradiance * (sss_w * spike + (1.0f - sss_w) * gaussian(sss_sigma_far[1], r));
        diffusion_profile[4 * i + 2] =
            irradiance * (sss_w * spike + (1.0f - sss_w) * gaussian(sss_sigma_far[2], r));

        diffusion_profile[4 * i + 0] = std::pow(diffusion_profile[4 * i + 0], 0.454f);
        diffusion_profile[4 * i + 1] = std::pow(diffusion_profile[4 * i + 1], 0.454f);
        diffusion_profile[4 * i + 2] = std::pow(diffusion_profile[4 * i + 2], 0.454f);
        diffusion_profile[4 * i + 3] = 1.0f;
    }

    gfx::texture_2d_upload_data(texture, diffusion_profile.data());
}

void generate_mip_cubemap(const GLuint program, const gfx::Texture2D &longlat_envmap,
                          gfx::TextureCubemap &cubemap)
{
    // Backup viewport
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    // Initialize cubemap
    cubemap.target = GL_TEXTURE_CUBE_MAP;
    cubemap.width = 128;
    cubemap.height = 128;
    cubemap.max_level = 7;
    cubemap.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 4};
    cubemap.sampling = {GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_REPEAT};
    gfx::texture_cubemap_create(cubemap);
    assert(cubemap.texture != 0);

    // Render the longitude-latitude envmap into the cubemap
    const std::vector<glm::mat4> rotations = {
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)),   // +X
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)),  // -X
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),  // +Y
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),   // -Y
        glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)),    // +Z
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f))}; // -Z

    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDisable(GL_CULL_FACE);

    GLuint fbo;
    glCreateFramebuffers(1, &fbo);
    glViewport(0, 0, cubemap.width, cubemap.height);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glBindTextureUnit(0, longlat_envmap.texture);
    glUseProgram(program);
    for (int32_t face = 0; face < 6; ++face) {
        glNamedFramebufferTextureLayer(fbo, GL_COLOR_ATTACHMENT0, cubemap.texture, 0, face);
        assert(glCheckNamedFramebufferStatus(fbo, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
        glNamedFramebufferDrawBuffer(fbo, GL_COLOR_ATTACHMENT0);
        glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &rotations[face][0][0]);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

    // Generate mipmaps
    glGenerateTextureMipmap(cubemap.texture);
}

} // namespace utils
