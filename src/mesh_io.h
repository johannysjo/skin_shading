#pragma once

#include <vector>

namespace utils {

void load_obj_mesh(const char *filename, std::vector<float> &vertices, std::vector<float> &normals,
                   std::vector<float> &texcoords);

} // namespace utils