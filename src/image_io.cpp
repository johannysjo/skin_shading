#include "image_io.h"

#include "gfx.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef NDEBUG
#include <cassert>
#include <stdint.h>

namespace utils {

void load_rgba8_texture(const char *filename, gfx::Texture2D &texture, const bool mipmap)
{
    int32_t width;
    int32_t height;
    int32_t num_channels;
    uint8_t *data = stbi_load(filename, &width, &height, &num_channels, 4);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
    const GLuint min_filter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
    texture.sampling = {min_filter, GL_LINEAR, GL_MIRRORED_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    if (mipmap) {
        glGenerateTextureMipmap(texture.texture);
    }

    stbi_image_free(data);
}

void load_rgba16_texture(const char *filename, gfx::Texture2D &texture)
{
    int32_t width;
    int32_t height;
    int32_t num_channels;
    uint16_t *data = stbi_load_16(filename, &width, &height, &num_channels, 4);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_MIRRORED_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);
}

void load_r16_texture(const char *filename, gfx::Texture2D &texture)
{
    int32_t width;
    int32_t height;
    int32_t num_channels;
    uint16_t *data = stbi_load_16(filename, &width, &height, &num_channels, 1);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_R16, GL_RED, GL_UNSIGNED_SHORT, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_MIRRORED_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);
}

// Loads a longitude-latitude HDR environment map into a texture
void load_hdr_envmap(const char *filename, gfx::Texture2D &texture)
{
    int32_t width;
    int32_t height;
    int32_t num_channels;
    float *data = stbi_loadf(filename, &width, &height, &num_channels, 4);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA32F, GL_RGBA, GL_FLOAT, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);
}

} // namespace utils