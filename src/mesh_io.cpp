#include "mesh_io.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <stdint.h>
#include <string>

namespace {

// Calculates per-vertex normals as the area-weighted average of adjacent face normals
void calculate_per_vertex_normals(const tinyobj::attrib_t &attrib,
                                  const std::vector<tinyobj::shape_t> &shapes,
                                  std::vector<float> &normals)
{
    assert(attrib.vertices.size() % 3 == 0);

    std::vector<float> attrib_normals(attrib.vertices.size(), 0.0f);
    for (const auto &shape : shapes) {
        assert(shape.mesh.indices.size() % 3 == 0);
        for (uint64_t i = 0; i < shape.mesh.indices.size(); i += 3) {
            const auto &idx0 = shape.mesh.indices[i + 0];
            const auto &idx1 = shape.mesh.indices[i + 1];
            const auto &idx2 = shape.mesh.indices[i + 2];

            // Fetch the three vertices v0, v1, and v2 of the current face
            const float v0x = attrib.vertices[3 * idx0.vertex_index + 0];
            const float v0y = attrib.vertices[3 * idx0.vertex_index + 1];
            const float v0z = attrib.vertices[3 * idx0.vertex_index + 2];

            const float v1x = attrib.vertices[3 * idx1.vertex_index + 0];
            const float v1y = attrib.vertices[3 * idx1.vertex_index + 1];
            const float v1z = attrib.vertices[3 * idx1.vertex_index + 2];

            const float v2x = attrib.vertices[3 * idx2.vertex_index + 0];
            const float v2y = attrib.vertices[3 * idx2.vertex_index + 1];
            const float v2z = attrib.vertices[3 * idx2.vertex_index + 2];

            // Calculate the face normal n as n = (v1 - v0) x (v2 - v0)
            const float nx = (v1y - v0y) * (v2z - v0z) - (v1z - v0z) * (v2y - v0y);
            const float ny = (v1z - v0z) * (v2x - v0x) - (v1x - v0x) * (v2z - v0z);
            const float nz = (v1x - v0x) * (v2y - v0y) - (v1y - v0y) * (v2x - v0x);

            // Add the unnormalized face normal to the per-vertex normals
            attrib_normals[3 * idx0.vertex_index + 0] += nx;
            attrib_normals[3 * idx0.vertex_index + 1] += ny;
            attrib_normals[3 * idx0.vertex_index + 2] += nz;

            attrib_normals[3 * idx1.vertex_index + 0] += nx;
            attrib_normals[3 * idx1.vertex_index + 1] += ny;
            attrib_normals[3 * idx1.vertex_index + 2] += nz;

            attrib_normals[3 * idx2.vertex_index + 0] += nx;
            attrib_normals[3 * idx2.vertex_index + 1] += ny;
            attrib_normals[3 * idx2.vertex_index + 2] += nz;
        }
    }

    for (const auto &shape : shapes) {
        for (const auto &idx : shape.mesh.indices) {
            float nx = attrib_normals[3 * idx.vertex_index + 0];
            float ny = attrib_normals[3 * idx.vertex_index + 1];
            float nz = attrib_normals[3 * idx.vertex_index + 2];

            // Normalize
            const float n_length = std::sqrt(nx * nx + ny * ny + nz * nz);
            if (n_length > 0.0f) {
                nx /= n_length;
                ny /= n_length;
                nz /= n_length;
            }

            normals.push_back(nx);
            normals.push_back(ny);
            normals.push_back(nz);
        }
    }
}

} // namespace

namespace utils {

// TODO: currently the loaded mesh is returned as triangle soup, should change that so the function
// returns an indexed mesh instead
void load_obj_mesh(const char *filename, std::vector<float> &vertices, std::vector<float> &normals,
                   std::vector<float> &texcoords)
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn;
    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename);
    if (!warn.empty()) {
        std::printf("%s\n", warn.c_str());
    }
    if (!err.empty()) {
        std::printf("%s\n", err.c_str());
    }
    assert(ret);

    assert(attrib.vertices.size() % 3 == 0);
    assert(attrib.texcoords.size() % 2 == 0);
    for (const auto &shape : shapes) {
        for (const auto &idx : shape.mesh.indices) {
            const float vx = attrib.vertices[3 * idx.vertex_index + 0];
            const float vy = attrib.vertices[3 * idx.vertex_index + 1];
            const float vz = attrib.vertices[3 * idx.vertex_index + 2];
            const float tx = attrib.texcoords[2 * idx.texcoord_index + 0];
            const float ty = 1.0 - attrib.texcoords[2 * idx.texcoord_index + 1];

            vertices.push_back(vx);
            vertices.push_back(vy);
            vertices.push_back(vz);
            texcoords.push_back(tx);
            texcoords.push_back(ty);
        }
    }

    calculate_per_vertex_normals(attrib, shapes, normals);
}

} // namespace utils