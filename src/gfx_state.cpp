#include "gfx_state.h"

#include <GL/glew.h>

namespace gfx {

GLuint to_gl_enum(const PolygonMode value)
{
    switch (value) {
    case POLYGON_MODE_FILL: {
        return GL_FILL;
    }
    case POLYGON_MODE_LINE: {
        return GL_LINE;
    }
    case POLYGON_MODE_POINT: {
        return GL_POINT;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const CullMode value)
{
    switch (value) {
    case CULL_MODE_FRONT: {
        return GL_FRONT;
    }
    case CULL_MODE_BACK: {
        return GL_BACK;
    }
    case CULL_MODE_FRONT_AND_BACK: {
        return GL_FRONT_AND_BACK;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const FrontFace value)
{
    switch (value) {
    case FRONT_FACE_COUNTER_CLOCKWISE: {
        return GL_CCW;
    }
    case FRONT_FACE_CLOCKWISE: {
        return GL_CW;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const CompareOp value)
{
    switch (value) {
    case COMPARE_OP_NEVER: {
        return GL_NEVER;
    }
    case COMPARE_OP_LESS: {
        return GL_LESS;
    }
    case COMPARE_OP_EQUAL: {
        return GL_EQUAL;
    }
    case COMPARE_OP_LEQUAL: {
        return GL_LEQUAL;
    }
    case COMPARE_OP_GREATER: {
        return GL_GREATER;
    }
    case COMPARE_OP_NOTEQUAL: {
        return GL_NOTEQUAL;
    }
    case COMPARE_OP_GEQUAL: {
        return GL_GEQUAL;
    }
    case COMPARE_OP_ALWAYS: {
        return GL_ALWAYS;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const StencilOp value)
{
    switch (value) {
    case STENCIL_OP_KEEP: {
        return GL_KEEP;
    }
    case STENCIL_OP_ZERO: {
        return GL_ZERO;
    }
    case STENCIL_OP_REPLACE: {
        return GL_REPLACE;
    }
    case STENCIL_OP_INCREMENT_AND_CLAMP: {
        return GL_INCR;
    }
    case STENCIL_OP_INCREMENT_AND_WRAP: {
        return GL_INCR_WRAP;
    }
    case STENCIL_OP_INVERT: {
        return GL_INVERT;
    }
    case STENCIL_OP_DECREMENT_AND_CLAMP: {
        return GL_DECR;
    }
    case STENCIL_OP_DECREMENT_AND_WRAP: {
        return GL_DECR_WRAP;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const BlendFactor value)
{
    switch (value) {
    case BLEND_FACTOR_ZERO: {
        return GL_ZERO;
    }
    case BLEND_FACTOR_ONE: {
        return GL_ONE;
    }
    case BLEND_FACTOR_SRC_COLOR: {
        return GL_SRC_COLOR;
    }
    case BLEND_FACTOR_ONE_MINUS_SRC_COLOR: {
        return GL_ONE_MINUS_SRC_COLOR;
    }
    case BLEND_FACTOR_DST_COLOR: {
        return GL_DST_COLOR;
    }
    case BLEND_FACTOR_ONE_MINUS_DST_COLOR: {
        return GL_ONE_MINUS_DST_COLOR;
    }
    case BLEND_FACTOR_SRC_ALPHA: {
        return GL_SRC_ALPHA;
    }
    case BLEND_FACTOR_ONE_MINUS_SRC_ALPHA: {
        return GL_ONE_MINUS_SRC_ALPHA;
    }
    case BLEND_FACTOR_DST_ALPHA: {
        return GL_DST_ALPHA;
    }
    case BLEND_FACTOR_ONE_MINUS_DST_ALPHA: {
        return GL_ONE_MINUS_DST_ALPHA;
    }
    case BLEND_FACTOR_CONSTANT_COLOR: {
        return GL_CONSTANT_COLOR;
    }
    case BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR: {
        return GL_ONE_MINUS_CONSTANT_COLOR;
    }
    case BLEND_FACTOR_CONSTANT_ALPHA: {
        return GL_CONSTANT_ALPHA;
    }
    case BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA: {
        return GL_ONE_MINUS_CONSTANT_ALPHA;
    }
    case BLEND_FACTOR_SRC_ALPHA_SATURATE: {
        return GL_SRC_ALPHA_SATURATE;
    }
    case BLEND_FACTOR_SRC1_COLOR: {
        return GL_SRC1_COLOR;
    }
    case BLEND_FACTOR_ONE_MINUS_SRC1_COLOR: {
        return GL_ONE_MINUS_SRC1_COLOR;
    }
    case BLEND_FACTOR_SRC1_ALPHA: {
        return GL_SRC1_ALPHA;
    }
    case BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA: {
        return GL_ONE_MINUS_SRC1_ALPHA;
    }
    default: {
        return 0;
    }
    }
}

GLuint to_gl_enum(const BlendOp value)
{
    switch (value) {
    case BLEND_OP_ADD: {
        return GL_FUNC_ADD;
    }
    case BLEND_OP_SUBTRACT: {
        return GL_FUNC_SUBTRACT;
    }
    case BLEND_OP_REVERSE_SUBTRACT: {
        return GL_FUNC_REVERSE_SUBTRACT;
    }
    case BLEND_OP_MIN: {
        return GL_MIN;
    }
    case BLEND_OP_MAX: {
        return GL_MAX;
    }
    default: {
        return 0;
    }
    }
}

GLboolean to_gl_bool(const bool value)
{
    if (value) {
        return GL_TRUE;
    }
    else {
        return GL_FALSE;
    }
}

void toggle_gl_cap(bool enabled, GLuint cap)
{
    if (enabled) {
        glEnable(cap);
    }
    else {
        glDisable(cap);
    }
}

void set_gl_rasterization_state(const RasterizationStateInfo &new_state,
                                const RasterizationStateInfo &current_state, bool force = false)
{
    if (new_state.polygon_mode != current_state.polygon_mode || force) {
        glPolygonMode(GL_FRONT_AND_BACK, to_gl_enum(new_state.polygon_mode));
    }

    if (new_state.culling_enabled != current_state.culling_enabled || force) {
        toggle_gl_cap(new_state.culling_enabled, GL_CULL_FACE);
    }

    if (new_state.cull_mode != current_state.cull_mode || force) {
        glCullFace(to_gl_enum(new_state.cull_mode));
    }

    if (new_state.front_face != current_state.front_face || force) {
        glFrontFace(to_gl_enum(new_state.front_face));
    }
}

void set_gl_depth_stencil_state(const DepthStencilStateInfo &new_state,
                                const DepthStencilStateInfo &current_state, bool force = false)
{
    // Depth
    if (new_state.depth_test_enabled != current_state.depth_test_enabled || force) {
        toggle_gl_cap(new_state.depth_test_enabled, GL_DEPTH_TEST);
    }

    if (new_state.depth_write_enabled != current_state.depth_write_enabled || force) {
        glDepthMask(to_gl_bool(new_state.depth_write_enabled));
    }

    if (new_state.depth_compare_op != current_state.depth_compare_op || force) {
        glDepthFunc(to_gl_enum(new_state.depth_compare_op));
    }

    // Stencil
    if (new_state.stencil_test_enabled != current_state.stencil_test_enabled || force) {
        toggle_gl_cap(new_state.stencil_test_enabled, GL_STENCIL_TEST);
    }

    // Stencil front
    if (new_state.front.fail_op != current_state.front.fail_op ||
        new_state.front.pass_op != current_state.front.pass_op ||
        new_state.front.depth_fail_op != current_state.front.depth_fail_op || force) {
        glStencilOpSeparate(GL_FRONT, to_gl_enum(new_state.front.fail_op),
                            to_gl_enum(new_state.front.pass_op),
                            to_gl_enum(new_state.front.depth_fail_op));
    }
    if (new_state.front.compare_op != current_state.front.compare_op ||
        new_state.front.write_mask != current_state.front.write_mask ||
        new_state.front.reference != current_state.front.reference || force) {
        glStencilFuncSeparate(GL_FRONT, to_gl_enum(new_state.front.compare_op),
                              new_state.front.reference, new_state.front.write_mask);
    }

    // Stencil back
    if (new_state.back.fail_op != current_state.back.fail_op ||
        new_state.back.pass_op != current_state.back.pass_op ||
        new_state.back.depth_fail_op != current_state.back.depth_fail_op || force) {
        glStencilOpSeparate(GL_BACK, to_gl_enum(new_state.back.fail_op),
                            to_gl_enum(new_state.back.pass_op),
                            to_gl_enum(new_state.back.depth_fail_op));
    }
    if (new_state.back.compare_op != current_state.back.compare_op ||
        new_state.back.write_mask != current_state.back.write_mask ||
        new_state.back.reference != current_state.back.reference || force) {
        glStencilFuncSeparate(GL_BACK, to_gl_enum(new_state.back.compare_op),
                              new_state.back.reference, new_state.back.write_mask);
    }
}

void set_gl_color_blend_state(const ColorBlendStateInfo &new_state,
                              const ColorBlendStateInfo &current_state, bool force = false)
{
    if (new_state.blend_enabled != current_state.blend_enabled || force) {
        toggle_gl_cap(new_state.blend_enabled, GL_BLEND);
    }

    if (new_state.src_color_blend_factor != current_state.src_color_blend_factor ||
        new_state.dst_color_blend_factor != current_state.dst_color_blend_factor ||
        new_state.src_alpha_blend_factor != current_state.src_alpha_blend_factor ||
        new_state.dst_alpha_blend_factor != current_state.dst_alpha_blend_factor || force) {
        glBlendFuncSeparate(to_gl_enum(new_state.src_color_blend_factor),
                            to_gl_enum(new_state.dst_color_blend_factor),
                            to_gl_enum(new_state.src_alpha_blend_factor),
                            to_gl_enum(new_state.dst_alpha_blend_factor));
    }

    if (new_state.color_blend_op != current_state.color_blend_op ||
        new_state.alpha_blend_op != current_state.alpha_blend_op || force) {
        glBlendEquationSeparate(to_gl_enum(new_state.color_blend_op),
                                to_gl_enum(new_state.alpha_blend_op));
    }

    if (new_state.color_write_mask != current_state.color_write_mask || force) {
        GLboolean red = (new_state.color_write_mask & COLOR_COMPONENT_R_BIT) != 0;
        GLboolean green = (new_state.color_write_mask & COLOR_COMPONENT_G_BIT) != 0;
        GLboolean blue = (new_state.color_write_mask & COLOR_COMPONENT_B_BIT) != 0;
        GLboolean alpha = (new_state.color_write_mask & COLOR_COMPONENT_A_BIT) != 0;
        glColorMask(red, green, blue, alpha);
    }
}

void set_render_state(const RenderStateInfo &new_state, RenderStateInfo &current_state, bool force)
{
    // Synchronize OpenGL render state with incoming new render state.
    set_gl_rasterization_state(new_state.rasterization, current_state.rasterization, force);
    set_gl_depth_stencil_state(new_state.depth_stencil, current_state.depth_stencil, force);
    set_gl_color_blend_state(new_state.color_blend, current_state.color_blend, force);

    // Store new render state as current.
    current_state.rasterization = new_state.rasterization;
    current_state.depth_stencil = new_state.depth_stencil;
    current_state.color_blend = new_state.color_blend;
}

} // namespace gfx
