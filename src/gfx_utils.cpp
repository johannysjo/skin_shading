#include "gfx_utils.h"

#include <glm/vec3.hpp>

#include <cmath>

namespace gfx {

void trackball_start_tracking(Trackball &trackball, const glm::vec2 &point)
{
    trackball.p = point;
    trackball.tracking = true;
}

void trackball_move(Trackball &trackball, const glm::vec2 &point)
{
    const glm::vec2 motion = point - trackball.p;
    trackball.p = point;
    if (std::abs(motion[0]) < 1.0f && std::abs(motion[1]) < 1.0f) {
        return;
    }

    const float theta_x =
        (trackball.speed * std::fmax(-trackball.clamp, std::fmin(trackball.clamp, motion[0])));
    const float theta_y =
        (trackball.speed * std::fmax(-trackball.clamp, std::fmin(trackball.clamp, motion[1])));

    const glm::quat delta_x = glm::angleAxis(theta_x, glm::vec3(0.0f, 1.0f, 0.0f));
    const glm::quat delta_y = glm::angleAxis(theta_y, glm::vec3(1.0f, 0.0f, 0.0f));

    const glm::quat tmp = glm::normalize(glm::cross(delta_y, delta_x));
    trackball.q = glm::normalize(glm::cross(tmp, trackball.q));

    // Quaternion should be in the positive hemisphere
    if (trackball.q[0] < 0.0f) {
        trackball.q = -trackball.q;
    }
}

void trackball_stop_tracking(Trackball &trackball)
{
    trackball.tracking = false;
}

glm::mat4 trackball_get_rotation_matrix(const Trackball &trackball)
{
    return glm::mat4_cast(trackball.q);
}

} // namespace gfx
