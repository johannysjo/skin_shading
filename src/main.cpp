/// @file
/// @brief Subsurface scattering demo.
///
/// This rendering demo implements screen-space subsurface scattering (SSSSS), a technique that can
/// be used to produce realistic renderings of translucent materials such as skin, wax, or marble.
/// The demo features the following rendering techniques:
/// - Deferred shading
/// - SSSSS
/// - Thick-object translucency (via shadow mapping)
/// - Simplified GGX shading model with normalized diffuse wrap
/// - Soft shadows (shadow mapping)
/// - Screen-space ambient occlusion (SSAO)
/// - Depth of field
/// - Temporal anti-aliasing (TAA). Color clamping only, no reprojection.
/// - Contrast adaptive sharpening (CAS)
///
/// @section LICENSE
///
/// Copyright (C) 2019  Johan Nysjö
///
/// This software is distributed under the MIT license. See the included LICENSE.txt file for
/// more details.

#include "gfx.h"
#include "gfx_utils.h"
#include "image_io.h"
#include "logging.h"
#include "mesh_io.h"
#include "utils.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/trigonometric.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#undef NDEBUG
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

enum ToneMappingOp : uint32_t {
    TONE_MAPPING_OP_ACES,
    TONE_MAPPING_OP_GAMMA_ONLY,
    TONE_MAPPING_OP_NONE
};

enum TimeStamp : uint32_t {
    TIME_STAMP_GBUFFER,
    TIME_STAMP_SHADOW_MAPS,
    TIME_STAMP_DEPTH_DOWNSAMPLING,
    TIME_STAMP_SSAO,
    TIME_STAMP_SHADING,
    TIME_STAMP_SSS,
    TIME_STAMP_DOF,
    TIME_STAMP_TAA,
    TIME_STAMP_BLOOM,
    TIME_STAMP_TONE_MAPPING,
    TIME_STAMP_CAS,
    TIME_STAMP_TOTAL,
    NUM_TIME_STAMPS
};

struct Window {
    GLFWwindow *handle = nullptr;
    uint32_t width = 1024;
    uint32_t height = 768;
    uint32_t frame_index = 0;
};

struct TranslucencySettings {
    bool enabled = true;
    glm::vec3 color = {1.0f, 0.5f, 0.5f};
    float max_depth = 0.06f;
};

struct SSSSettings {
    bool enabled = true;
    glm::vec3 sigma_far = {1.0f, 0.6f, 0.5f};
    float scale = 2.0f;
    float w = 0.075f;
    uint32_t num_samples = 5;
};

struct ShadowsSettings {
    bool enabled = true;
    float normal_bias = 0.0015f;
    glm::vec2 slope_bias = {0.0025, 0.0075f};
    float pdf_radius = 4.0f;
    uint32_t pdf_num_samples = 4;
};

struct SSAOSettings {
    bool enabled = true;
    float radius = 0.01f;
    float sphere_offset = 0.9f;
    uint32_t num_samples = 4;
};

struct DepthOfFieldSettings {
    bool enabled = true;
    float focus_distance = 0.7f;
    float radius_pixels = 4.0f;
    uint32_t num_samples = 6;
};

struct BloomSettings {
    bool enabled = true;
    float intensity = 0.05;
    uint32_t num_iterations = 2;
};

struct Settings {
    float gloss = 0.4f;
    float wrap = 0.35f;
    float bump_map_z_scale = 180.0f;
    glm::vec3 surface_tint_color = {1.0f, 1.0f, 1.0f};
    float emissive_strength = 0.0f;
    float sky_light_intensity = 0.2f;
    TranslucencySettings translucency;
    SSSSettings sss;
    ShadowsSettings shadows;
    SSAOSettings ssao;
    DepthOfFieldSettings dof;
    BloomSettings bloom;
    float exposure = 1.0f;
    ToneMappingOp tone_mapping_op = TONE_MAPPING_OP_ACES;
    bool taa_enabled = true;
    bool cas_enabled = false;
};

struct UISettings {
    bool show_settings_ui = true;
    ImVec2 settings_ui_size = {300, 710};
    ImVec2 settings_ui_pos = {10, 10};
    bool settings_ui_collapsed = false;

    bool show_profiler_ui = true;
    ImVec2 profiler_ui_size = {300, 310};
    ImVec2 profiler_ui_pos = {320, 10};
    bool profiler_ui_collapsed = true;
};

struct Context {
    Window window;
    GLuint timer_queries_tic[NUM_TIME_STAMPS];
    GLuint timer_queries_toc[NUM_TIME_STAMPS];
    float frame_times_ms[NUM_TIME_STAMPS];

    GLuint default_vao = 0;
    std::unordered_map<std::string, GLuint> programs{};
    std::unordered_map<std::string, gfx::FBO> fbos{};

    utils::MeshVAO mesh_vao{};
    utils::BoundingBox mesh_bbox{};
    glm::mat4 world_from_model;

    gfx::Texture2D base_color_texture;
    gfx::Texture2D metalness_texture;
    gfx::Texture2D bump_map_texture;
    gfx::Texture2D sss_mask_texture;
    gfx::Texture2D emissive_texture;
    gfx::Texture2D blue_noise_texture;
    gfx::Texture2D blue_noise_texture2;
    gfx::Texture2D diffusion_profile_texture;
    gfx::Texture2D envmap_texture;
    gfx::TextureCubemap cubemap_texture;
    bool cubemap_is_dirty = true;

    GLuint scene_ubo = 0;

    utils::Camera camera{};
    glm::mat4 view_from_world;
    glm::mat4 proj_from_view;
    glm::mat4 proj_from_world;
    glm::mat4 world_from_view;
    glm::mat4 view_from_proj;
    glm::mat4 world_from_proj;

    gfx::Trackball trackball;

    utils::DirectionalLight key_light{};
    glm::mat4 key_light_view_from_world;
    glm::mat4 key_light_proj_from_view;
    glm::mat4 key_light_proj_from_world;

    Settings settings;
    UISettings ui_settings;
};

std::string root_dir()
{
    const char *path = std::getenv("SKIN_SHADING_ROOT");
    const auto path_str = path != nullptr ? std::string(path) : std::string();
    if (path_str.empty()) {
        LOG_ERROR("SKIN_SHADING_ROOT is not set\n");
        std::exit(EXIT_FAILURE);
    }

    return path_str;
}

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename)
{
    std::ifstream vshader_ifs(vertex_shader_filename);
    std::stringstream vshader_ss;
    vshader_ss << vshader_ifs.rdbuf();

    std::ifstream fshader_ifs(fragment_shader_filename);
    std::stringstream fshader_ss;
    fshader_ss << fshader_ifs.rdbuf();

    GLuint program = gfx::create_shader_program(vshader_ss.str().c_str(), fshader_ss.str().c_str());

    return program;
}

GLuint load_shader_program(const std::string &compute_shader_filename)
{
    std::ifstream cshader_ifs(compute_shader_filename);
    std::stringstream cshader_ss;
    cshader_ss << cshader_ifs.rdbuf();

    GLuint program = gfx::create_shader_program(cshader_ss.str().c_str());

    return program;
}

void load_shaders(Context &ctx)
{
    // clang-format off
    const std::string shader_dir = root_dir() + "/src/shaders/";

    ctx.programs["gbuffer"] = load_shader_program(
        shader_dir + "gbuffer.vert",
        shader_dir + "gbuffer.frag");

    ctx.programs["shadow_map"] = load_shader_program(
        shader_dir + "shadow_map.vert",
        shader_dir + "shadow_map.frag");

    ctx.programs["downsample_depth"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "downsample_depth.frag");

    ctx.programs["ssao"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "ssao.frag");

    ctx.programs["shading"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "shading.frag");

    ctx.programs["sss_composite"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "sss_composite.frag");

    ctx.programs["background"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "background.frag");

    ctx.programs["dof_coc"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "dof_coc.frag");

    ctx.programs["dof"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "dof.frag");

    ctx.programs["taa"] = load_shader_program(
        shader_dir + "taa.comp");

    ctx.programs["bloom_brightpass"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "bloom_brightpass.frag");

    ctx.programs["gaussian_blur"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "gaussian_blur.frag");

    ctx.programs["tone_mapping"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "tone_mapping.frag");

    ctx.programs["cas"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "cas.frag");

    ctx.programs["blit"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "blit.frag");

    ctx.programs["longlat_to_cubemap"] = load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "longlat_to_cubemap.frag");
    // clang-format on
}

void create_fbos(Context &ctx)
{
    { // G-buffer
        gfx::Texture2D base_color_gloss_texture;
        base_color_gloss_texture.target = GL_TEXTURE_2D;
        base_color_gloss_texture.width = ctx.window.width;
        base_color_gloss_texture.height = ctx.window.height;
        base_color_gloss_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        base_color_gloss_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(base_color_gloss_texture);

        gfx::Texture2D normal_texture;
        normal_texture.target = GL_TEXTURE_2D;
        normal_texture.width = ctx.window.width;
        normal_texture.height = ctx.window.height;
        normal_texture.storage = {GL_RGB10_A2, GL_RGBA, GL_UNSIGNED_INT_10_10_10_2, 1};
        normal_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(normal_texture);

        gfx::Texture2D sss_emissive_texture;
        sss_emissive_texture.target = GL_TEXTURE_2D;
        sss_emissive_texture.width = ctx.window.width;
        sss_emissive_texture.height = ctx.window.height;
        sss_emissive_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        sss_emissive_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(sss_emissive_texture);

        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = ctx.window.width;
        depth_texture.height = ctx.window.height;
        depth_texture.storage = {GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, base_color_gloss_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, normal_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT2, sss_emissive_texture);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["gbuffer"] = fbo;
    }

    { // Shadow maps
        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = 768;
        depth_texture.height = 768;
        depth_texture.storage = {GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, depth_texture.width, depth_texture.height, false);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["shadow_map"] = fbo;
    }

    { // Half-res depth buffer
        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = ctx.window.width > 1 ? ctx.window.width / 2 : 1;
        depth_texture.height = ctx.window.height > 1 ? ctx.window.height / 2 : 1;
        depth_texture.storage = {GL_R32F, GL_RED, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, depth_texture.width, depth_texture.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["half_res_depth"] = fbo;
    }

    { // SSAO
        gfx::Texture2D ssao_texture;
        ssao_texture.target = GL_TEXTURE_2D;
        ssao_texture.width = ctx.window.width;
        ssao_texture.height = ctx.window.height;
        ssao_texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
        ssao_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(ssao_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, ssao_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["ssao"] = fbo;
    }

    { // Shading
        gfx::Texture2D diffuse_texture;
        diffuse_texture.target = GL_TEXTURE_2D;
        diffuse_texture.width = ctx.window.width;
        diffuse_texture.height = ctx.window.height;
        diffuse_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        diffuse_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(diffuse_texture);

        gfx::Texture2D specular_texture;
        specular_texture.target = GL_TEXTURE_2D;
        specular_texture.width = ctx.window.width;
        specular_texture.height = ctx.window.height;
        specular_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        specular_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(specular_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, diffuse_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, specular_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["shading"] = fbo;
    }

    { // SSS + composite
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["resolve"] = fbo;
    }

    { // Depth of field
        gfx::Texture2D coc_texture;
        coc_texture.target = GL_TEXTURE_2D;
        coc_texture.width = ctx.window.width;
        coc_texture.height = ctx.window.height;
        coc_texture.storage = {GL_R16F, GL_RED, GL_FLOAT, 1};
        coc_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(coc_texture);

        gfx::Texture2D dof_color_texture;
        dof_color_texture.target = GL_TEXTURE_2D;
        dof_color_texture.width = ctx.window.width;
        dof_color_texture.height = ctx.window.height;
        dof_color_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        dof_color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(dof_color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, coc_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, dof_color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["dof"] = fbo;
    }

    { // TAA
        gfx::Texture2D taa_history_texture;
        taa_history_texture.target = GL_TEXTURE_2D;
        taa_history_texture.width = ctx.window.width;
        taa_history_texture.height = ctx.window.height;
        taa_history_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        taa_history_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(taa_history_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, taa_history_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["taa"] = fbo;
    }

    { // Bloom
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width > 3 ? ctx.window.width / 4 : 1;
        color_texture.height = ctx.window.height > 3 ? ctx.window.height / 4 : 1;
        color_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);

        gfx::Texture2D color_texture2;
        color_texture2.target = GL_TEXTURE_2D;
        color_texture2.width = color_texture.width;
        color_texture2.height = color_texture.height;
        color_texture2.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture2.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture2);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, color_texture.width, color_texture.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, color_texture2);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["bloom"] = fbo;
    }

    { // Tone mapping
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["tone_mapping"] = fbo;
    }
}

void load_mesh(Context &ctx)
{
#if 1
    utils::Mesh mesh{};
    utils::load_obj_mesh((root_dir() + "/resources/lpshead/head.OBJ").c_str(), mesh.vertices,
                         mesh.normals, mesh.texcoords);
    ctx.mesh_bbox = utils::compute_bbox(mesh.vertices);
    ctx.mesh_vao = create_mesh_vao(mesh);

    utils::load_rgba8_texture((root_dir() + "/resources/lpshead/lambertian.jpg").c_str(),
                              ctx.base_color_texture, true);
    utils::load_r16_texture((root_dir() + "/resources/lpshead/bump.png").c_str(),
                            ctx.bump_map_texture);
    utils::create_solid_r8_texture(4, 4, 0, ctx.metalness_texture);
    utils::create_solid_r8_texture(4, 4, 255, ctx.sss_mask_texture);
    utils::create_solid_r8_texture(4, 4, 0, ctx.emissive_texture);
#else
    utils::Mesh mesh{};
    utils::load_obj_mesh((root_dir() + "/resources/frog/frog_edited.obj").c_str(), mesh.vertices,
                         mesh.normals, mesh.texcoords);
    ctx.mesh_bbox = utils::compute_bbox(mesh.vertices);
    ctx.mesh_vao = create_mesh_vao(mesh);

    utils::load_rgba8_texture((root_dir() + "/resources/frog/frog_base_color.png").c_str(),
                              ctx.base_color_texture, true);
    utils::load_rgba8_texture((root_dir() + "/resources/frog/frog_bump.png").c_str(),
                              ctx.bump_map_texture, false);
    utils::create_solid_r8_texture(4, 4, 0, ctx.metalness_texture);
    utils::create_solid_r8_texture(4, 4, 255, ctx.sss_mask_texture);
    utils::create_solid_r8_texture(4, 4, 0, ctx.emissive_texture);
#endif
}

void init_camera(Context &ctx)
{
    ctx.camera.eye = glm::vec3(0.0f, 0.0f, 0.7f);
    ctx.camera.center = glm::vec3(0.0f, 0.0f, 0.0f);
    ctx.camera.up = glm::vec3(0.0f, 1.0f, 0.0f);
    ctx.camera.fovy = glm::radians(45.0f);
    ctx.camera.aspect = float(ctx.window.width) / float(ctx.window.height);
    ctx.camera.z_near = 0.01f;
    ctx.camera.z_far = 100.0f;

    utils::fit_camera_to_bbox(ctx.camera, ctx.mesh_bbox);

    ctx.settings.dof.focus_distance = utils::get_auto_focus_distance(ctx.camera, ctx.mesh_bbox);
}

void init_light_sources(Context &ctx)
{
    ctx.key_light.direction = glm::vec3(-0.5f, -0.5f, 0.0f);
    ctx.key_light.color = glm::vec3(1.0f, 1.0f, 1.0f);
    ctx.key_light.intensity = 1.0f;
}

void init_timer_queries(Context &ctx)
{
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_tic);
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_toc);
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        ctx.frame_times_ms[i] = 0.0f;
    }
}

void init(Context &ctx)
{
    glCreateVertexArrays(1, &ctx.default_vao);
    load_shaders(ctx);
    create_fbos(ctx);

    load_mesh(ctx);

    utils::generate_diffusion_profile_texture(ctx.diffusion_profile_texture,
                                              ctx.settings.sss.sigma_far, ctx.settings.sss.w);

    utils::load_rgba8_texture(
        (root_dir() + "/resources/blue_noise_textures/64_64/LDR_RGBA_0.png").c_str(),
        ctx.blue_noise_texture, false);
    utils::load_rgba8_texture(
        (root_dir() + "/resources/blue_noise_textures/64_64/LDR_RGBA_1.png").c_str(),
        ctx.blue_noise_texture2, false);

    utils::load_hdr_envmap((root_dir() + "/resources/envmaps/autoshop_01_1k.hdr").c_str(),
                           ctx.envmap_texture);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    ctx.cubemap_is_dirty = true;

    init_camera(ctx);
    init_light_sources(ctx);
    init_timer_queries(ctx);
}

void update_settings_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui_settings.settings_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui_settings.settings_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui_settings.settings_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.5f);
    ImGui::Begin("Settings", &ctx.ui_settings.show_settings_ui);

    if (ImGui::CollapsingHeader("Material", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::SliderFloat("Gloss", &ctx.settings.gloss, 0.0f, 1.0f);
        ImGui::ColorEdit3("Surface tint color", &ctx.settings.surface_tint_color[0]);
        ImGui::SliderFloat("Bump scale", &ctx.settings.bump_map_z_scale, 1.0f, 500.0f, "%.4f");
        ImGui::SliderFloat("Wrap", &ctx.settings.wrap, 0.0f, 1.0f);
        ImGui::SliderFloat("Emissive strength", &ctx.settings.emissive_strength, 0.0f, 20.0f);

        ImGui::Checkbox("Translucency", &ctx.settings.translucency.enabled);
        ImGui::ColorEdit3("Transl. color", &ctx.settings.translucency.color[0]);
        ImGui::SliderFloat("Transl. depth", &ctx.settings.translucency.max_depth, 0.001f, 0.08f);

        ImGui::Checkbox("SSS", &ctx.settings.sss.enabled);
        if (ImGui::ColorEdit3("SSS sigma_far (mm)", &ctx.settings.sss.sigma_far[0],
                              ImGuiColorEditFlags_Float)) {
            utils::generate_diffusion_profile_texture(
                ctx.diffusion_profile_texture, ctx.settings.sss.sigma_far, ctx.settings.sss.w);
        }
        ImGui::SliderFloat("SSS scale", &ctx.settings.sss.scale, 1.0f, 5.0f);
        if (ImGui::SliderFloat("SSS w", &ctx.settings.sss.w, 0.0f, 1.0f)) {
            utils::generate_diffusion_profile_texture(
                ctx.diffusion_profile_texture, ctx.settings.sss.sigma_far, ctx.settings.sss.w);
        }
        ImGui::SliderInt("SSS #samples", (int32_t *)&ctx.settings.sss.num_samples, 1, 16);
        ImGui::Image((void *)ctx.diffusion_profile_texture.texture, ImVec2(200.0f, 20.0f),
                     ImVec2(0.0f, 0.0f), ImVec2(1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 1.0f),
                     ImVec4(0.1f, 0.1f, 0.1f, 1.0f));
    }

    if (ImGui::CollapsingHeader("Camera")) {
        ImGui::SliderFloat("Exposure", &ctx.settings.exposure, 0.0f, 5.0f);
        ImGui::Checkbox("DoF", &ctx.settings.dof.enabled);
        ImGui::SliderFloat("DoF focus distance", &ctx.settings.dof.focus_distance, 0.01f, 2.0f);
        ImGui::SliderFloat("DoF radius (pixels)", &ctx.settings.dof.radius_pixels, 0.0f, 20.0f);
        ImGui::SliderInt("DoF #samples", (int32_t *)&ctx.settings.dof.num_samples, 1, 16);
    }

    if (ImGui::CollapsingHeader("Lighting")) {
        ImGui::SliderFloat("Key light intensity", &ctx.key_light.intensity, 0.0f, 2.0f);
        ImGui::SliderFloat3("Key light direction", &ctx.key_light.direction[0], -5.0f, 5.0f);
        ImGui::SliderFloat("Sky light intensity", &ctx.settings.sky_light_intensity, 0.0f, 2.0f);
    }

    if (ImGui::CollapsingHeader("Shadows")) {
        ImGui::Checkbox("Shadows##shadows_enabled", &ctx.settings.shadows.enabled);
        ImGui::SliderFloat2("Slope bias", &ctx.settings.shadows.slope_bias[0], 0.0f, 0.02f, "%.4f");
        ImGui::SliderFloat("Normal bias", &ctx.settings.shadows.normal_bias, 0.0f, 0.005f, "%.4f");
        ImGui::SliderFloat("PDF radius", &ctx.settings.shadows.pdf_radius, 0.0f, 10.0f);
        ImGui::SliderInt("PDF #samples", (int32_t *)&ctx.settings.shadows.pdf_num_samples, 1, 16);

        ImGui::Checkbox("SSAO", &ctx.settings.ssao.enabled);
        ImGui::SliderFloat("SSAO radius", &ctx.settings.ssao.radius, 0.001f, 0.05f);
        ImGui::SliderFloat("SSAO offset", &ctx.settings.ssao.sphere_offset, 0.0f, 1.0f);
        ImGui::SliderInt("SSAO #samples", (int32_t *)&ctx.settings.ssao.num_samples, 1, 32);
    }

    if (ImGui::CollapsingHeader("PostFX")) {
        ImGui::Checkbox("TAA", &ctx.settings.taa_enabled);
        ImGui::Checkbox("Bloom", &ctx.settings.bloom.enabled);
        ImGui::SliderFloat("Bloom intensity", &ctx.settings.bloom.intensity, 0.0f, 1.0f);
        ImGui::SliderInt("Bloom #iterations", (int32_t *)&ctx.settings.bloom.num_iterations, 1, 5);
        ImGui::Combo("Tone mapping", (int32_t *)(&ctx.settings.tone_mapping_op),
                     "ACES\0Gamma-only\0None\0");
        ImGui::Checkbox("Contrast Adaptive Sharpening (CAS)", &ctx.settings.cas_enabled);
    }

    ImGui::End();
}

void profiler_bar(const char *label, const float time_in_ms, const ImVec4 &color)
{
    const float cursor_pos_x = ImGui::GetCursorPosX();
    ImGui::ColorButton("", color, 0, ImVec2(std::fmin(10.0f * time_in_ms, 1000.0f), 0.0f));
    ImGui::SameLine();
    ImGui::SetCursorPosX(cursor_pos_x + 2.0f);
    ImGui::Text("%s %.1f", label, time_in_ms);
}

void update_profiler_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui_settings.profiler_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui_settings.profiler_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui_settings.profiler_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.5f);
    ImGui::Begin("Frame times (ms)", &ctx.ui_settings.show_profiler_ui);

    const ImVec4 color(0.7f, 0.5f, 0.1f, 1.0f);
    profiler_bar("G-buffer", ctx.frame_times_ms[TIME_STAMP_GBUFFER], color);
    profiler_bar("Shadow maps", ctx.frame_times_ms[TIME_STAMP_SHADOW_MAPS], color);
    profiler_bar("Half-res depth", ctx.frame_times_ms[TIME_STAMP_DEPTH_DOWNSAMPLING], color);
    profiler_bar("SSAO", ctx.frame_times_ms[TIME_STAMP_SSAO], color);
    profiler_bar("Shading", ctx.frame_times_ms[TIME_STAMP_SHADING], color);
    profiler_bar("SSS + comp", ctx.frame_times_ms[TIME_STAMP_SSS], color);
    profiler_bar("DoF", ctx.frame_times_ms[TIME_STAMP_DOF], color);
    profiler_bar("TAA", ctx.frame_times_ms[TIME_STAMP_TAA], color);
    profiler_bar("Bloom", ctx.frame_times_ms[TIME_STAMP_BLOOM], color);
    profiler_bar("Tone mapping", ctx.frame_times_ms[TIME_STAMP_TONE_MAPPING], color);
    profiler_bar("CAS", ctx.frame_times_ms[TIME_STAMP_CAS], color);
    profiler_bar("Total", ctx.frame_times_ms[TIME_STAMP_TOTAL], color);

    ImGui::End();
}

void update_transforms(Context &ctx)
{
    ctx.world_from_model = glm::translate(glm::mat4(1.0f), ctx.camera.center) *
                           gfx::trackball_get_rotation_matrix(ctx.trackball) *
                           glm::translate(glm::mat4(1.0f), -ctx.camera.center);

    const glm::mat4 taa_offset_matrix = utils::get_taa_offset_matrix(
        ctx.window.width, ctx.window.height, ctx.window.frame_index, ctx.settings.taa_enabled);

    ctx.view_from_world = glm::lookAt(ctx.camera.eye, ctx.camera.center, ctx.camera.up);
    ctx.proj_from_view = taa_offset_matrix * glm::perspective(ctx.camera.fovy, ctx.camera.aspect,
                                                              ctx.camera.z_near, ctx.camera.z_far);
    ctx.proj_from_world = ctx.proj_from_view * ctx.view_from_world;
    ctx.world_from_view = glm::inverse(ctx.view_from_world);
    ctx.view_from_proj = glm::inverse(ctx.proj_from_view);
    ctx.world_from_proj = glm::inverse(ctx.proj_from_world);

    ctx.key_light_view_from_world =
        utils::get_light_view_from_world_matrix(ctx.key_light, ctx.mesh_bbox);
    ctx.key_light_proj_from_view = utils::get_light_proj_from_view_matrix(ctx.mesh_bbox);
    ctx.key_light_proj_from_world = ctx.key_light_proj_from_view * ctx.key_light_view_from_world;
}

void update_scene_ubo(Context &ctx)
{
    if (ctx.scene_ubo == 0) {
        glCreateBuffers(1, &ctx.scene_ubo);
        const uint32_t ubo_size = 6 * sizeof(glm::mat4) + sizeof(glm::vec4);
        glNamedBufferData(ctx.scene_ubo, ubo_size, nullptr, GL_DYNAMIC_DRAW);
    }

    uint32_t offset = 0;
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.view_from_world[0][0]);
    offset += sizeof(glm::mat4);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.proj_from_view[0][0]);
    offset += sizeof(glm::mat4);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.proj_from_world[0][0]);
    offset += sizeof(glm::mat4);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.world_from_view[0][0]);
    offset += sizeof(glm::mat4);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.view_from_proj[0][0]);
    offset += sizeof(glm::mat4);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::mat4), &ctx.world_from_proj[0][0]);
    offset += sizeof(glm::mat4);
    const auto camera_world_pos = glm::vec4(ctx.camera.eye, 1.0f);
    glNamedBufferSubData(ctx.scene_ubo, offset, sizeof(glm::vec4), &camera_world_pos[0]);
}

void update(Context &ctx)
{
    update_settings_ui(ctx);
    update_profiler_ui(ctx);
    update_transforms(ctx);
    update_scene_ubo(ctx);
}

void draw_gbuffer(const GLuint program, const utils::MeshVAO &vao,
                  const glm::mat4 &world_from_model, const gfx::Texture2D &base_color_texture,
                  const gfx::Texture2D &metalness_texture, const gfx::Texture2D &bump_map_texture,
                  const gfx::Texture2D &sss_mask_texture, const gfx::Texture2D &emissive_texture,
                  const GLuint scene_ubo, const float gloss, const float bump_map_z_scale,
                  const glm::vec3 tint_color, const float wrap, const float emissive_strength)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, scene_ubo);
    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &world_from_model[0][0]);
    glProgramUniform1f(program, 1, gloss);
    glProgramUniform1f(program, 2, bump_map_z_scale);
    glProgramUniform3fv(program, 3, 1, &tint_color[0]);
    glProgramUniform1f(program, 4, wrap);
    glProgramUniform1f(program, 5, emissive_strength);

    glBindTextureUnit(0, base_color_texture.texture);
    glBindTextureUnit(1, metalness_texture.texture);
    glBindTextureUnit(2, bump_map_texture.texture);
    glBindTextureUnit(3, sss_mask_texture.texture);
    glBindTextureUnit(4, emissive_texture.texture);

    glUseProgram(program);
    glBindVertexArray(vao.vao);
    glDrawArrays(GL_TRIANGLES, 0, vao.num_vertices);
}

void draw_shadow_map(const GLuint program, const utils::MeshVAO &vao,
                     const glm::mat4 &world_from_model, const glm::mat4 &view_from_world,
                     const glm::mat4 &proj_from_view)
{
    const glm::mat4 proj_from_model = proj_from_view * view_from_world * world_from_model;
    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &proj_from_model[0][0]);

    glUseProgram(program);
    glBindVertexArray(vao.vao);
    glDrawArrays(GL_TRIANGLES, 0, vao.num_vertices);
}

void draw_downsample_depth(const GLuint program, const gfx::Texture2D &depth_texture)
{
    glBindTextureUnit(0, depth_texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_ssao(const GLuint program, const gfx::Texture2D &depth_texture,
               const gfx::Texture2D &half_res_depth_texture,
               const gfx::Texture2D &world_normal_texture, const gfx::Texture2D &blue_noise_texture,
               const GLuint scene_ubo, const uint32_t frame_index,
               const SSAOSettings &ssao_settings)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, scene_ubo);
    glProgramUniform1ui(program, 0, frame_index);
    glProgramUniform1f(program, 1, ssao_settings.radius);
    glProgramUniform1f(program, 2, ssao_settings.sphere_offset);
    glProgramUniform1ui(program, 3, ssao_settings.num_samples);

    glBindTextureUnit(0, depth_texture.texture);
    glBindTextureUnit(1, half_res_depth_texture.texture);
    glBindTextureUnit(2, world_normal_texture.texture);
    glBindTextureUnit(3, blue_noise_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_shading(const GLuint program, const gfx::Texture2D &gbuffer_base_color_gloss_texture,
                  const gfx::Texture2D &gbuffer_normal_texture,
                  const gfx::Texture2D &gbuffer_sss_emissive_texture,
                  const gfx::Texture2D &gbuffer_depth_texture, const gfx::Texture2D &ssao_texture,
                  const gfx::Texture2D &shadow_map_texture,
                  const gfx::Texture2D &blue_noise_texture, const GLuint scene_ubo,
                  const uint32_t frame_index, const utils::DirectionalLight &key_light,
                  const glm::mat4 &key_light_proj_from_world,
                  const glm::mat4 &key_light_proj_from_view, const Settings &settings,
                  const gfx::TextureCubemap &cubemap_texture)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, scene_ubo);
    glProgramUniform3fv(program, 0, 1, &key_light.direction[0]);
    glProgramUniform1f(program, 1, key_light.intensity);
    glProgramUniformMatrix4fv(program, 2, 1, GL_FALSE, &key_light_proj_from_world[0][0]);
    glProgramUniformMatrix4fv(program, 3, 1, GL_FALSE, &key_light_proj_from_view[0][0]);
    glProgramUniform1f(program, 4, settings.sky_light_intensity);
    glProgramUniform1f(program, 5, settings.exposure);
    glProgramUniform1i(program, 6, settings.shadows.enabled);
    glProgramUniform2fv(program, 7, 1, &settings.shadows.slope_bias[0]);
    glProgramUniform1f(program, 8, settings.shadows.normal_bias);
    glProgramUniform1f(program, 9, settings.shadows.pdf_radius);
    glProgramUniform1ui(program, 10, settings.shadows.pdf_num_samples);
    glProgramUniform3fv(program, 11, 1, &settings.translucency.color[0]);
    glProgramUniform1i(program, 12, settings.translucency.enabled);
    glProgramUniform1f(program, 13, settings.translucency.max_depth);
    glProgramUniform1ui(program, 14, frame_index);

    glBindTextureUnit(0, gbuffer_base_color_gloss_texture.texture);
    glBindTextureUnit(1, gbuffer_normal_texture.texture);
    glBindTextureUnit(2, gbuffer_sss_emissive_texture.texture);
    glBindTextureUnit(3, gbuffer_depth_texture.texture);
    glBindTextureUnit(4, ssao_texture.texture);
    glBindTextureUnit(5, shadow_map_texture.texture);
    glBindTextureUnit(6, blue_noise_texture.texture);
    glBindTextureUnit(7, cubemap_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_sss_composite(const GLuint program, const gfx::Texture2D &diffuse_texture,
                        const gfx::Texture2D &specular_texture, const gfx::Texture2D &depth_texture,
                        const gfx::Texture2D &blue_noise_texture,
                        const gfx::Texture2D &base_color_texture,
                        const gfx::Texture2D &sss_emissive_texture, const GLuint scene_ubo,
                        const uint32_t frame_index, const SSSSettings &sss_settings,
                        const float exposure)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, scene_ubo);
    glProgramUniform1ui(program, 0, frame_index);
    glProgramUniform1f(program, 1, sss_settings.scale);
    glProgramUniform1ui(program, 2, sss_settings.num_samples);
    glProgramUniform1ui(program, 3, sss_settings.enabled);
    glProgramUniform3fv(program, 4, 1, &sss_settings.sigma_far[0]);
    glProgramUniform1f(program, 5, sss_settings.w);
    glProgramUniform1f(program, 6, exposure);

    glBindTextureUnit(0, diffuse_texture.texture);
    glBindTextureUnit(1, depth_texture.texture);
    glBindTextureUnit(2, blue_noise_texture.texture);
    glBindTextureUnit(3, base_color_texture.texture);
    glBindTextureUnit(4, specular_texture.texture);
    glBindTextureUnit(5, sss_emissive_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_background(const GLuint program, const gfx::Texture2D &depth_texture,
                     const gfx::TextureCubemap &cubemap_texture, const GLuint scene_ubo,
                     const float intensity, const float exposure)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, scene_ubo);
    glProgramUniform1f(program, 0, intensity);
    glProgramUniform1f(program, 1, exposure);

    glBindTextureUnit(0, depth_texture.texture);
    glBindTextureUnit(1, cubemap_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_dof_coc(const GLuint program, const gfx::Texture2D &depth_texture,
                  const DepthOfFieldSettings &dof_settings, const float z_near, const float z_far)
{
    glProgramUniform1f(program, 0, dof_settings.focus_distance);
    glProgramUniform1f(program, 1, z_near);
    glProgramUniform1f(program, 2, z_far);

    glBindTextureUnit(0, depth_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_dof(const GLuint program, const gfx::Texture2D &color_texture,
              const gfx::Texture2D &coc_texture, const gfx::Texture2D &blue_noise_texture,
              const uint32_t frame_index, const DepthOfFieldSettings &dof_settings)
{
    glProgramUniform1ui(program, 0, frame_index);
    glProgramUniform1f(program, 1, dof_settings.radius_pixels);
    glProgramUniform1ui(program, 2, dof_settings.num_samples);

    glBindTextureUnit(0, color_texture.texture);
    glBindTextureUnit(1, coc_texture.texture);
    glBindTextureUnit(2, blue_noise_texture.texture);

    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void dispatch_taa(const GLuint program, const gfx::Texture2D &current_texture,
                  const gfx::Texture2D &history_texture)
{
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glBindTextureUnit(0, current_texture.texture);
    glBindImageTexture(1, history_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glUseProgram(program);
    glDispatchCompute(history_texture.width / 8 + 1, history_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void draw_bloom_brightpass(const GLuint program, const gfx::Texture2D &color_texture,
                           const float bloom_intensity)
{
    glProgramUniform1f(program, 0, bloom_intensity);
    glBindTextureUnit(0, color_texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_gaussian_blur(const GLuint program, const gfx::Texture2D &texture,
                        const glm::vec2 &direction)
{
    glProgramUniform2fv(program, 0, 1, &direction[0]);
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_tone_mapping(const GLuint program, const gfx::Texture2D &texture,
                       const ToneMappingOp tone_mapping_op)
{
    glProgramUniform1ui(program, 0, tone_mapping_op);
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_cas(const GLuint program, const gfx::Texture2D &texture)
{
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_blit(const GLuint program, const gfx::Texture2D &texture)
{
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void reset_gl_states()
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glDisable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);
}

void update_frame_times(Context &ctx)
{
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        GLuint64 tic_ns;
        glGetQueryObjectui64v(ctx.timer_queries_tic[i], GL_QUERY_RESULT, &tic_ns);
        GLuint64 toc_ns;
        glGetQueryObjectui64v(ctx.timer_queries_toc[i], GL_QUERY_RESULT, &toc_ns);
        const auto elapsed_time_ms = toc_ns > tic_ns ? float(toc_ns - tic_ns) / 1.0e6f : 0.0f;
        ctx.frame_times_ms[i] = 0.2f * elapsed_time_ms + 0.8f * ctx.frame_times_ms[i];
    }
}

void display(Context &ctx)
{
    glBindVertexArray(ctx.default_vao);
    reset_gl_states();

    if (ctx.cubemap_is_dirty) {
        utils::generate_mip_cubemap(ctx.programs["longlat_to_cubemap"], ctx.envmap_texture,
                                    ctx.cubemap_texture);
        ctx.cubemap_is_dirty = false;
        reset_gl_states();
    }

    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TOTAL], GL_TIMESTAMP);

    // G-buffer
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_GBUFFER], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        GLenum targets[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
        glNamedFramebufferDrawBuffers(ctx.fbos["gbuffer"].fbo, 3, targets);
        glm::vec4 clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["gbuffer"].fbo, GL_COLOR, 0, &clear_value[0]);
        glClearNamedFramebufferfv(ctx.fbos["gbuffer"].fbo, GL_COLOR, 1, &clear_value[0]);
        glClearNamedFramebufferfv(ctx.fbos["gbuffer"].fbo, GL_COLOR, 2, &clear_value[0]);
        glClearNamedFramebufferfi(ctx.fbos["gbuffer"].fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["gbuffer"].fbo);
        draw_gbuffer(ctx.programs["gbuffer"], ctx.mesh_vao, ctx.world_from_model,
                     ctx.base_color_texture, ctx.metalness_texture, ctx.bump_map_texture,
                     ctx.sss_mask_texture, ctx.emissive_texture, ctx.scene_ubo, ctx.settings.gloss,
                     ctx.settings.bump_map_z_scale, ctx.settings.surface_tint_color,
                     ctx.settings.wrap, ctx.settings.emissive_strength);
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_GBUFFER], GL_TIMESTAMP);

    // Shadow maps
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_SHADOW_MAPS], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glClearNamedFramebufferfi(ctx.fbos["shadow_map"].fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);

        if (ctx.settings.shadows.enabled) {
            glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["shadow_map"].fbo);
            glViewport(0, 0, ctx.fbos["shadow_map"].width, ctx.fbos["shadow_map"].height);
            draw_shadow_map(ctx.programs["shadow_map"], ctx.mesh_vao, ctx.world_from_model,
                            ctx.key_light_view_from_world, ctx.key_light_proj_from_view);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            reset_gl_states();
        }
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_SHADOW_MAPS], GL_TIMESTAMP);

    // Depth buffer downsampling
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_DEPTH_DOWNSAMPLING], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glViewport(0, 0, ctx.fbos["half_res_depth"].width, ctx.fbos["half_res_depth"].height);
        glNamedFramebufferDrawBuffer(ctx.fbos["half_res_depth"].fbo, GL_COLOR_ATTACHMENT0);
        glm::vec4 clear_value = {1.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["half_res_depth"].fbo, GL_COLOR, 0, &clear_value[0]);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["half_res_depth"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        draw_downsample_depth(ctx.programs["downsample_depth"],
                              ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT]);
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_DEPTH_DOWNSAMPLING], GL_TIMESTAMP);

    // SSAO
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_SSAO], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glNamedFramebufferDrawBuffer(ctx.fbos["ssao"].fbo, GL_COLOR_ATTACHMENT0);
        glm::vec4 clear_value = {1.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["ssao"].fbo, GL_COLOR, 0, &clear_value[0]);

        if (ctx.settings.ssao.enabled) {
            glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["ssao"].fbo);
            glDisable(GL_DEPTH_TEST);
            glDepthMask(GL_FALSE);
            glDisable(GL_CULL_FACE);
            draw_ssao(ctx.programs["ssao"], ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT],
                      ctx.fbos["half_res_depth"].attachments[GL_COLOR_ATTACHMENT0],
                      ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT1], ctx.blue_noise_texture,
                      ctx.scene_ubo, ctx.window.frame_index, ctx.settings.ssao);
            reset_gl_states();
        }
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_SSAO], GL_TIMESTAMP);

    // Shading
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_SHADING], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        GLenum targets[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
        glNamedFramebufferDrawBuffers(ctx.fbos["shading"].fbo, 2, targets);
        glm::vec4 clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["shading"].fbo, GL_COLOR, 0, &clear_value[0]);
        glClearNamedFramebufferfv(ctx.fbos["shading"].fbo, GL_COLOR, 1, &clear_value[0]);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["shading"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        draw_shading(ctx.programs["shading"], ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT0],
                     ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT1],
                     ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT2],
                     ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT],
                     ctx.fbos["ssao"].attachments[GL_COLOR_ATTACHMENT0],
                     ctx.fbos["shadow_map"].attachments[GL_DEPTH_ATTACHMENT],
                     ctx.blue_noise_texture, ctx.scene_ubo, ctx.window.frame_index, ctx.key_light,
                     ctx.key_light_proj_from_world, ctx.key_light_proj_from_view, ctx.settings,
                     ctx.cubemap_texture);
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_SHADING], GL_TIMESTAMP);

    // SSS + composite
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_SSS], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glNamedFramebufferDrawBuffer(ctx.fbos["resolve"].fbo, GL_COLOR_ATTACHMENT0);
        glm::vec4 clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["resolve"].fbo, GL_COLOR, 0, &clear_value[0]);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["resolve"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        draw_sss_composite(
            ctx.programs["sss_composite"], ctx.fbos["shading"].attachments[GL_COLOR_ATTACHMENT0],
            ctx.fbos["shading"].attachments[GL_COLOR_ATTACHMENT1],
            ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT], ctx.blue_noise_texture2,
            ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT0],
            ctx.fbos["gbuffer"].attachments[GL_COLOR_ATTACHMENT2], ctx.scene_ubo,
            ctx.window.frame_index, ctx.settings.sss, ctx.settings.exposure);

        draw_background(ctx.programs["background"],
                        ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT], ctx.cubemap_texture,
                        ctx.scene_ubo, ctx.settings.sky_light_intensity, ctx.settings.exposure);

        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_SSS], GL_TIMESTAMP);

    // Depth-of-field
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_DOF], GL_TIMESTAMP);
    if (ctx.settings.dof.enabled) {
        glBindVertexArray(ctx.default_vao);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);

        glNamedFramebufferDrawBuffer(ctx.fbos["dof"].fbo, GL_COLOR_ATTACHMENT0);
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["dof"].fbo);
        draw_dof_coc(ctx.programs["dof_coc"], ctx.fbos["gbuffer"].attachments[GL_DEPTH_ATTACHMENT],
                     ctx.settings.dof, ctx.camera.z_near, ctx.camera.z_far);

        glNamedFramebufferReadBuffer(ctx.fbos["dof"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["dof"].fbo, GL_COLOR_ATTACHMENT1);
        draw_dof(ctx.programs["dof"], ctx.fbos["resolve"].attachments[GL_COLOR_ATTACHMENT0],
                 ctx.fbos["dof"].attachments[GL_COLOR_ATTACHMENT0], ctx.blue_noise_texture2,
                 ctx.window.frame_index, ctx.settings.dof);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["resolve"].fbo);
        draw_blit(ctx.programs["blit"], ctx.fbos["dof"].attachments[GL_COLOR_ATTACHMENT1]);

        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_DOF], GL_TIMESTAMP);

    // TAA
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TAA], GL_TIMESTAMP);
    if (ctx.settings.taa_enabled) {
        dispatch_taa(ctx.programs["taa"], ctx.fbos["resolve"].attachments[GL_COLOR_ATTACHMENT0],
                     ctx.fbos["taa"].attachments[GL_COLOR_ATTACHMENT0]);

        glNamedFramebufferReadBuffer(ctx.fbos["taa"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["resolve"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["taa"].fbo, ctx.fbos["resolve"].fbo, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TAA], GL_TIMESTAMP);

    // Bloom
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BLOOM], GL_TIMESTAMP);
    if (ctx.settings.bloom.enabled) {
        glBindVertexArray(ctx.default_vao);
        glViewport(0, 0, ctx.fbos["bloom"].width, ctx.fbos["bloom"].height);

        glNamedFramebufferReadBuffer(ctx.fbos["resolve"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT0);
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["bloom"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        draw_bloom_brightpass(ctx.programs["bloom_brightpass"],
                              ctx.fbos["resolve"].attachments[GL_COLOR_ATTACHMENT0],
                              ctx.settings.bloom.intensity);

        for (uint32_t i = 0; i < ctx.settings.bloom.num_iterations; ++i) {
            glNamedFramebufferReadBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT0);
            glNamedFramebufferDrawBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT1);
            draw_gaussian_blur(ctx.programs["gaussian_blur"],
                               ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                               glm::vec2(1.0f, 0.0f));

            glNamedFramebufferReadBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT1);
            glNamedFramebufferDrawBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT0);
            draw_gaussian_blur(ctx.programs["gaussian_blur"],
                               ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT1],
                               glm::vec2(0.0f, 1.0f));
        }

        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glNamedFramebufferReadBuffer(ctx.fbos["bloom"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["resolve"].fbo, GL_COLOR_ATTACHMENT0);
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["resolve"].fbo);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        draw_blit(ctx.programs["blit"], ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0]);

        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BLOOM], GL_TIMESTAMP);

    // Tone mapping
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glNamedFramebufferDrawBuffer(ctx.fbos["tone_mapping"].fbo, GL_COLOR_ATTACHMENT0);
        glm::vec4 clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(ctx.fbos["tone_mapping"].fbo, GL_COLOR, 0, &clear_value[0]);

        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["tone_mapping"].fbo);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        draw_tone_mapping(ctx.programs["tone_mapping"],
                          ctx.fbos["resolve"].attachments[GL_COLOR_ATTACHMENT0],
                          ctx.settings.tone_mapping_op);
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);

    // Contrast Adaptive Sharpening (CAS) + resolve
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_CAS], GL_TIMESTAMP);
    {
        glBindVertexArray(ctx.default_vao);
        glm::vec4 clear_value = {0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(0, GL_COLOR, 0, &clear_value[0]);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glDisable(GL_CULL_FACE);
        if (ctx.settings.cas_enabled) {
            draw_cas(ctx.programs["cas"],
                     ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0]);
        }
        else {
            draw_blit(ctx.programs["blit"],
                      ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0]);
        }
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_CAS], GL_TIMESTAMP);

    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TOTAL], GL_TIMESTAMP);

    update_frame_times(ctx);

    glBindFramebuffer(GL_FRAMEBUFFER, 0); // required for ImGui
}

void error_callback(const int /*error*/, const char *description)
{
    LOG_ERROR("%s\n", description);
}

void key_callback(GLFWwindow *window, const int key, const int scancode, const int action,
                  const int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) {
        return;
    }

    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        load_shaders(*ctx);
    }
}

void mouse_button_callback(GLFWwindow *window, const int button, const int action, const int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        gfx::trackball_start_tracking(ctx->trackball, glm::vec2(x, y));
    }
    else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        gfx::trackball_stop_tracking(ctx->trackball);
    }
}

void cursor_pos_callback(GLFWwindow *window, const double x, const double y)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (ctx->trackball.tracking) {
        gfx::trackball_move(ctx->trackball, glm::vec2(x, y));
    }
}

void scroll_callback(GLFWwindow *window, const double x_offset, const double y_offset)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_ScrollCallback(window, x_offset, y_offset);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    const float zoom_step = glm::radians(1.0f);
    if (y_offset > 0.0) {
        ctx->camera.fovy = std::fmax(glm::radians(1.0f), ctx->camera.fovy - zoom_step);
    }
    else if (y_offset < 0.0) {
        ctx->camera.fovy = std::fmin(glm::radians(180.0f), ctx->camera.fovy + zoom_step);
    }
}

void resize_fbos(Context &ctx, const int width, const int height)
{
    for (auto &fbo : ctx.fbos) {
        gfx::fbo_resize(fbo.second, width, height);
    }

    const uint32_t half_width = width > 1 ? width / 2 : 1;
    const uint32_t half_height = height > 1 ? height / 2 : 1;
    gfx::fbo_resize(ctx.fbos["half_res_depth"], half_width, half_height);

    const uint32_t quarter_width = width > 3 ? width / 4 : 1;
    const uint32_t quarter_height = height > 3 ? height / 4 : 1;
    gfx::fbo_resize(ctx.fbos["bloom"], quarter_width, quarter_height);
}

void framebuffer_size_callback(GLFWwindow *window, const int width, const int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.width = width;
    ctx->window.height = height;
    ctx->camera.aspect = float(width) / float(height);
    resize_fbos(*ctx, width, height);
    glViewport(0, 0, width, height);
}

int main()
{
    Context ctx;

    // Initialize GLFW
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        std::exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    ctx.window.handle =
        glfwCreateWindow(ctx.window.width, ctx.window.height, "Skin shading", nullptr, nullptr);
    assert(ctx.window.handle != nullptr);
    glfwMakeContextCurrent(ctx.window.handle);

    glfwSetWindowUserPointer(ctx.window.handle, &ctx);
    glfwSetKeyCallback(ctx.window.handle, key_callback);
    glfwSetMouseButtonCallback(ctx.window.handle, mouse_button_callback);
    glfwSetCursorPosCallback(ctx.window.handle, cursor_pos_callback);
    glfwSetScrollCallback(ctx.window.handle, scroll_callback);
    glfwSetFramebufferSizeCallback(ctx.window.handle, framebuffer_size_callback);

    // Initialize GLEW
    glewExperimental = true;
    GLenum glewStatus = glewInit();
    if (glewStatus != GLEW_OK) {
        LOG_ERROR("%s\n", glewGetErrorString(glewStatus));
        std::exit(EXIT_FAILURE);
    }
    LOG_INFO("OpenGL version: %s\n", glGetString(GL_VERSION));

    // Initialize rendering
    init(ctx);

    // Initialize ImGui
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(ctx.window.handle, false);
    ImGui_ImplOpenGL3_Init("#version 450");

    // Start rendering loop
    while (!glfwWindowShouldClose(ctx.window.handle)) {
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        update(ctx);
        display(ctx);
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(ctx.window.handle);
        ctx.window.frame_index += 1;
    }

    // Shut down everything
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(ctx.window.handle);
    glfwTerminate();

    return EXIT_SUCCESS;
}
