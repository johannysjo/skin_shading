/// @file
/// @brief GFX render state tracking.
///
/// @section LICENSE
///
/// Copyright (C) 2017  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <stdint.h>

namespace gfx {

enum PolygonMode : uint32_t { POLYGON_MODE_FILL, POLYGON_MODE_LINE, POLYGON_MODE_POINT };

enum CullMode : uint32_t { CULL_MODE_FRONT, CULL_MODE_BACK, CULL_MODE_FRONT_AND_BACK };

enum FrontFace : uint32_t { FRONT_FACE_COUNTER_CLOCKWISE, FRONT_FACE_CLOCKWISE };

struct RasterizationStateInfo {
    PolygonMode polygon_mode = POLYGON_MODE_FILL;
    bool culling_enabled = true;
    CullMode cull_mode = CULL_MODE_BACK;
    FrontFace front_face = FRONT_FACE_COUNTER_CLOCKWISE;
};

enum CompareOp : uint32_t {
    COMPARE_OP_NEVER,
    COMPARE_OP_LESS,
    COMPARE_OP_EQUAL,
    COMPARE_OP_LEQUAL,
    COMPARE_OP_GREATER,
    COMPARE_OP_NOTEQUAL,
    COMPARE_OP_GEQUAL,
    COMPARE_OP_ALWAYS
};

enum StencilOp : uint32_t {
    STENCIL_OP_KEEP,
    STENCIL_OP_ZERO,
    STENCIL_OP_REPLACE,
    STENCIL_OP_INCREMENT_AND_CLAMP,
    STENCIL_OP_INCREMENT_AND_WRAP,
    STENCIL_OP_INVERT,
    STENCIL_OP_DECREMENT_AND_CLAMP,
    STENCIL_OP_DECREMENT_AND_WRAP
};

struct StencilOpState {
    StencilOp fail_op = STENCIL_OP_KEEP;
    StencilOp pass_op = STENCIL_OP_KEEP;
    StencilOp depth_fail_op = STENCIL_OP_KEEP;
    CompareOp compare_op = COMPARE_OP_ALWAYS;
    uint32_t write_mask = 1;
    uint32_t reference = 0;
};

struct DepthStencilStateInfo {
    bool depth_test_enabled = true;
    bool depth_write_enabled = true;
    CompareOp depth_compare_op = COMPARE_OP_LESS;
    bool stencil_test_enabled = false;
    StencilOpState front;
    StencilOpState back;
};

enum BlendFactor : uint32_t {
    BLEND_FACTOR_ZERO,
    BLEND_FACTOR_ONE,
    BLEND_FACTOR_SRC_COLOR,
    BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
    BLEND_FACTOR_DST_COLOR,
    BLEND_FACTOR_ONE_MINUS_DST_COLOR,
    BLEND_FACTOR_SRC_ALPHA,
    BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
    BLEND_FACTOR_DST_ALPHA,
    BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
    BLEND_FACTOR_CONSTANT_COLOR,
    BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR,
    BLEND_FACTOR_CONSTANT_ALPHA,
    BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA,
    BLEND_FACTOR_SRC_ALPHA_SATURATE,
    BLEND_FACTOR_SRC1_COLOR,
    BLEND_FACTOR_ONE_MINUS_SRC1_COLOR,
    BLEND_FACTOR_SRC1_ALPHA,
    BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA
};

enum BlendOp : uint32_t {
    BLEND_OP_ADD,
    BLEND_OP_SUBTRACT,
    BLEND_OP_REVERSE_SUBTRACT,
    BLEND_OP_MIN,
    BLEND_OP_MAX
};

enum ColorComponent : uint32_t {
    COLOR_COMPONENT_R_BIT = 1 << 0,
    COLOR_COMPONENT_G_BIT = 1 << 1,
    COLOR_COMPONENT_B_BIT = 1 << 2,
    COLOR_COMPONENT_A_BIT = 1 << 3
};

struct ColorBlendStateInfo {
    bool blend_enabled = false;
    BlendFactor src_color_blend_factor = BLEND_FACTOR_ONE;
    BlendFactor dst_color_blend_factor = BLEND_FACTOR_ZERO;
    BlendOp color_blend_op = BLEND_OP_ADD;
    BlendFactor src_alpha_blend_factor = BLEND_FACTOR_ONE;
    BlendFactor dst_alpha_blend_factor = BLEND_FACTOR_ZERO;
    BlendOp alpha_blend_op = BLEND_OP_ADD;
    uint32_t color_write_mask = (COLOR_COMPONENT_R_BIT | COLOR_COMPONENT_G_BIT |
                                 COLOR_COMPONENT_B_BIT | COLOR_COMPONENT_A_BIT);
};

struct RenderStateInfo {
    RasterizationStateInfo rasterization;
    DepthStencilStateInfo depth_stencil;
    ColorBlendStateInfo color_blend;
};

void set_render_state(const RenderStateInfo &new_state, RenderStateInfo &current_state,
                      bool force = false);

} // namespace gfx
