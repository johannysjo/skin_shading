#pragma once

namespace gfx { // forward declarations
struct Texture2D;
} // namespace gfx

namespace utils {

void load_rgba8_texture(const char *filename, gfx::Texture2D &texture, bool mipmap);

void load_rgba16_texture(const char *filename, gfx::Texture2D &texture);

void load_r16_texture(const char *filename, gfx::Texture2D &texture);

void load_hdr_envmap(const char *filename, gfx::Texture2D &texture);

} // namespace utils