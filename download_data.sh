#!/bin/bash
#
# This script will download the LPS Head 3D scan model (originally provided by
# Infinite-Realities) from Morgan McGuire's computer graphics archive. Run the
# script once from the project root folder before running build.sh.

cd resources && \
wget "https://casual-effects.com/g3d/data10/research/model/lpshead/lpshead.zip" && \
unzip lpshead.zip -d lpshead && \
rm lpshead.zip
