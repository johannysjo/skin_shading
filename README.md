# Subsurface scattering demo
This rendering demo implements screen-space subsurface scattering (SSS, or SSSSS), a technique that can be used to produce realistic renderings of translucent materials such as skin, wax, or marble. The demo is implemented in C++ and OpenGL 4.5 and features the following rendering techniques:

- Deferred shading
- Screen-space subsurface scattering
- Thick-object translucency (via shadow mapping)
- Simplified GGX shading model with normalized diffuse wrap
- Soft shadows (shadow mapping)
- Screen-space ambient occlusion (SSAO) with fake diffuse bounce
- Depth of field
- HDR lighting with bloom
- Temporal anti-aliasing (TAA). Color clamping only, no reprojection.
- Contrast adaptive sharpening (CAS)
- Filmic tone mapping

The basic idea of screen-space SSS is to simulate the scattering and absorption events that occurs in the subsurface medium by convolving the diffuse lighting (irradiance) texture with a diffusion profile. The diffusion profile used in this demo is composed of a spike and three Gaussians (one per color channel), similar to the one proposed by M.S. Mikkelsen in the paper Skin Rendering by Pseudo–Separable Cross Bilateral Filtering, 2010. Convolution is performed with well-distributed sampling points in a Vogel disk, where a tiled blue noise texture is used for randomly rotating and jittering the Vogel disk between pixels. This produces a somewhat noisy result, which is then cleaned up by the TAA.

## Example screenshot
![Screenshot](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot.png "Screenshot")

The screenshot shows the LPS 3D head scan model from Infinite-Realities, distributed by Morgan McGuire and Guedis Cardenas at http://graphics.cs.williams.edu/data/.

## Compiling (Linux)
Clone the git repository and run (from the root folder)
```bash
$ ./download_data.sh
$ ./build.sh
```
Requires CMake 3.0 or higher and a C++11 capable compiler (GCC 4.6+).

## License
The source code is provided under the MIT license. See LICENSE.txt for more information.

## More screenshots
Example showing how the screen-space SSS filter is used to blur the texture containing the diffuse lighting (irradiance). The final image is then obtained by multiplying the filtered diffuse lighting with the albedo and adding the specular lighting on top.

![SSS filter](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot_sss_diffuse_specular.png)

Frog model rendered with subsurface scattering:

![Frog with SSS](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot_frog_sss_on.png)

The same frog model rendered without subsurface scattering:

![Frog without SSS](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot_frog_sss_off.png)

The head model illuminated by two different HDR environment maps and one directional light source:

![Envmaps](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot_sss_envmaps.png)

ImGui user interface:

![ImGui user interface](https://bitbucket.org/johannysjo/skin_shading/raw/master/resources/screenshot_ui.png)
